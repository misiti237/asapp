async function preparaModifica() {

	var selected_element = null;
	$('body').on('mousedown', '.item_element', function(e) {

		if (!onMobile) {
			if (selected_element == null) selected_element = $(this);
		} else {
			//e.stopImmediatePropagation();
			console.log("firing");
			if (selected_element == null) {
				console.log("null");
				selected_element = $(this);
			} else {
				console.log("not null");
				$('.item_element').css({
					'opacity': '',
					'width': '',
					'height': ''
				});
				$('body').off('click', '.planimetria');
				if ($(selected_element).attr('data-id') == $(this).attr('data-id')) {
					console.log('selected_element == $(this) TRUE', $(selected_element).attr('data-id'));
					selected_element = null;
				} else {
					console.log('selected_element == $(this) FALSE');
					selected_element = $(this);
				}

			}
		}
	});

	//INSERIMENTO ELEMENTO INIZIO ---
	var check_time_item_dragscroll = false;
	var time_item_dragscroll = null;
	$('body').on('mousedown', '.items_table', function(e) {
		clearTimeout(time_item_dragscroll);
		time_item_dragscroll = setTimeout(function() {
			check_time_item_dragscroll = true;
		}, 500);
	});
	var activated = false; //LA VARIABILE NON VIENE USATA MA C'ERA .suca.cazzituoi.
	$('body').on('mouseup', '.items_table', function(e) { //C'E' SOLO IL MOUSEUP PERCHE' SU CHROME SE SI METTE ANCHE IL TOUCHEND NON FUNZIONA .suca.
		clearTimeout(time_item_dragscroll);
		if ((!check_time_item_dragscroll) && (selected_element != null) && (!activated)) {
			//activated = true;
			var backimg = $(selected_element).css("background-image");
			var id = $(selected_element).attr("data-id");
			var cat = $(selected_element).attr("data-cat");
			$('.item_new_element').css({
				'background-image': '' + backimg + ''
			});
			if (onMobile()) {
				//console.log("on mobile");
				$(selected_element).css({
					'opacity': '1',
					'width': '50px',
					'height': '50px'
				});

				$('body').on('click', '.toolbar', function() {
					selected_element = null;

					$('.item_element').css({
						'opacity': '',
						'width': '',
						'height': ''
					});
					$('body').off('click', '.planimetria');

					$('body').off('click', '.toolbar');
				});
			} else {
				console.log(id);
				$('.item_new_element').css({
					'display': 'block',
					'top': getLayerY(e) + 'px',
					'left': getLayerX(e) + 'px'
					// 'background-image': ''+backimg+''
				});

				$('body').on('mousemove', '.modifica', function(e) {
					$('.item_new_element').css({
						'top': (getLayerY(e) + 1) + 'px',
						'left': (getLayerX(e) + 1) + 'px'
					});
				});
			}
			$(".item_new_element").attr("data-id", id);
			$(".item_new_element").attr("data-cat", cat);

			$('body').on('click', '.planimetria', async function(e) { //SICCOME DEVE FUNZIONARE SOLO IN MODIFICA AGGIORNARE LA METODOLOGIA DI CONTROLLO .suca.cazzituoi.
				console.log('CLICK ON PLANIMETRIA');


				var offX = $('.workspace').offset().left + $('.planimetria').position().left;
				var offY = $('.workspace').offset().top + $('.planimetria').position().top;
				var mouseX = getLayerX(e) - offX;
				var mouseY = getLayerY(e) - offY;

				mouseX = (mouseX * 100) / $('.planimetria').width();
				mouseY = (mouseY * 100) / $('.planimetria').height();
				//qua controllo sul codice
				// if(await popup("Immetere codice strumento <input type='text' id='codice_str' value='' placeholder='codice'>", "Avanti", "Indietro")){
				var index = elements.length;
				var backimg = $(".item_new_element").css("background-image").replace('url(\"', '').replace('\")', '');
				//alert($(".item_new_element").css("background-image"));
				var idCategoria = $(".item_new_element").attr('data-id');
				var categoria = $(".item_new_element").attr('data-cat');
				var idPlitems = $(".planimetria").data()["id"];
				// var codice = $("#codice_str").val();
				console.log("AGGIUNTO ELEMENTO : ",index);
				elements[index] = new Element(index, mouseX, mouseY, backimg, idPlitems, null, null, null, idCategoria, null, null, zoom_perc, categoria);
				//++elements.length;
				console.log("NUOVA LUNGHEZZA:", elements.length);
				// }

				//activated = false;

				$('body').off('mousemove', '.modifica');
				//$('body').off('click', '.planimetria');
				check_time_item_dragscroll = false;
				time_item_dragscroll = null;

				if (!onMobile()) {
					selected_element = null;
					$('body').off('click', '.planimetria');

					$('.item_new_element').css({
						'display': '',
						'top': '',
						'left': ''
					});
				}
				//inizializzo dopo questa chiamata così non ho delay grafici
				var attributi = await call('items_attributi', idCategoria);
				console.log(attributi);
				elements[index].attributi = attributi;

			});
		}
		check_time_item_dragscroll = false;
	});
	//INSERIMENTO ELEMENTO FINE ---

	//DRAG AND DROP INIZIO ---

	// GESTIONE
	$('body').on('mousedown touchstart', '.item_rappr', function(e) {

		if (!list_page['modifica']) return;
		if (onMobile()) {
			$('.workspace').css({
				'overflow': 'hidden'
			});
		}
		drag_check = true;
		var element = $(this);
		console.log($(this).css('background-image'));
		off = new Array();
		off['x'] = $('.workspace').offset().left + $('.planimetria').position().left;
		off['y'] = $('.workspace').offset().top + $('.planimetria').position().top;

		$('body').on('mousemove touchmove', '.planimetria', function(e) {

			var mouse_position = new Array();
			mouse_position['x'] = getLayerX(e) - off['x'];
			mouse_position['y'] = getLayerY(e) - off['y'];

			//console.log(mouse_position['x']+' '+mouse_position['y']);
			var index_selected_item = $(element).attr('data-id');
			elements[index_selected_item].move(mouse_position['x'], mouse_position['y'], $('.planimetria'));
		});
	});

	$('body').on('mouseup touchend', '.planimetria', function(e) {
		if (!list_page['modifica']) return;
		if (onMobile()) {
			$('.workspace').css({
				'overflow': 'auto'
			});
		}
		first_pos = null;
		off = null;
		drag_check = false;
		$('body').off('mousemove touchmove', '.planimetria');
	});
	$('body').on('mouseleave', '.planimetria', function(e) {
		if (!list_page['modifica']) return;
		if (onMobile()) {
			$('.workspace').css({
				'overflow': 'auto'
			});
		}
		first_pos = null;
		off = null;
		drag_check = false;
		$('body').off('mousemove touchmove', '.planimetria');
	});
	// ********

	//DRAG AND DROP FINE ---

	//GESTIONE FRECCIE SELEZIONE ITEM INIZIO ---
	$('body').on('click touch', '.arrow_left', function() { //ARROW LEFT ONCLICK
		$('.items_table').css({
			'left': ($('.items_table').position().left + 80) + 'px'
		});
		if ($('.items_table').position().left >= 0) {
			$('.items_table').css({
				'left': '0px'
			});
		}
		aggiornaFreccieItem();
	});
	$('body').on('click touch', '.arrow_rigth', function() { //ARROW RIGHT ONCLICK
		$('.items_table').css({
			'left': ($('.items_table').position().left - 80) + 'px'
		});
		if ((($('.items_table').innerWidth() - ($('.items').innerWidth() + 20)) + $('.items_table').position().left) <= 0) {
			$('.items_table').css({
				'left': (($('.items_table').innerWidth() - ($('.items').innerWidth() - 20)) * (-1)) + 'px'
			});
		}
		aggiornaFreccieItem();
	});
	// GESTIONE FRECCIE SELEZIONE ITEM FINE ---

	// ELIMINAZIONE ITEM ---
	var check_elimina = false;
	$('body').on('click touch', '.toolbar_elimina', function() {
		console.log(check_elimina);
		if (!check_elimina) {
			$('.toolbar_elimina svg .cls-1').css({
				'fill': '#ca9797'
			});
			$('body').on('click touch', '.item_rappr', async function() {
				if (await popup('Sei sicuro di voler eliminare l\'item?', 'SI', 'NO')) {
					var index = Number($(this).attr('data-id'));
					elements[index] = null;
					$(this).remove();
				}
			});

			$('body').on('click touch', '.item_element', async function() {
				$('.toolbar_elimina svg .cls-1').css({
					'fill': ''
				});
				$('body').off('click touch', '.item_rappr');
				$('body').off('click touch', '.item_element');

				check_elimina = false;
			});
		} else {
			console.log("bianco");
			$('.toolbar_elimina svg .cls-1').css({
				'fill': ''
			});
			$('body').off('click touch', '.item_rappr');
			$('body').off('click touch', '.item_element');
		}
		//$('body').off('click touch', '.toolbar_elimina');
		check_elimina = !check_elimina;
	});

	// RESIZE ITEMS ---
	var check_resize = false;
	$('body').on('click touch', '.toolbar_resize', function() {
		if (!check_resize) {
			$('.popup_resize').animate({
				'top': ($('body').innerHeight() - ($('.popup_resize').innerHeight() + 20)) + 'px'
			}, 'fast');
			$('.toolbar_resize svg .cls-1').css({
				'fill': '#aac2e0'
			});

			$('body').on('change input blur', '.resize_value', function() {
				zoom_perc = $(this).val();

				$('.item_rappr').css({
					'width': zoom_perc + '%',
					'padding-top': zoom_perc + '%'
				});
			});
		} else {
			$('.popup_resize').animate({
				'top': ($('body').innerHeight() + 20) + 'px'
			}, 'fast');
			$('.toolbar_resize svg .cls-1').css({
				'fill': ''
			});
			$('body').off('change input blur', '.resize_value');
		}
		check_resize = !check_resize;
	});

}

//funzioni per gestire gli elementi nell'area modifica planimetria

//le funzioni verranno chiamate nel file main.js nella funzione indirizza modifica_planimetria

// <td><div class="item_element" style="background-image: url(....)" data-elemento="...."></div></td>
var edifici;

var elements = [] // .toppa.
function indirizzaModifica() {
	mascheraEdifici();
	elements.length = 0;
}

async function mascheraFiltri() {
	var code = '';
	var layers = await getCategorie();
	console.log(layers);

	for (var i = 0; i < layers.length; i++) {
		code += '<div class="toolbar_layer" data-id="' + layers[i]['id'] + '" style="background-image: url(' + layers[i]['src_icona'] + ')"></div>'
	}

	return code;
}

async function apriModifica(idPlitems, piano) {

	var content_html;
	if (onMobile()) {

		content_html = '<div class="header"><svg class="go_back_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 408 408"><style>.freccia{fill: #464646;}</style><title>go_back</title><path class="freccia" d="M408,178.5H96.9L239.7,35.7,204,0,0,204,204,408l35.7-35.7L96.9,229.5H408Z"/></svg><svg class="save_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.save{fill:#464646;}</style></defs><title>save</title><rect class="save" x="287.35" y="52.24" width="62.69" height="104.49"/><path class="save" d="M417.06,0H5.22V512H506.78V89.71ZM109.71,20.9H381.39V188.08H109.71Zm303,470.2H88.82V271.67H412.73Z"/></svg><div class="piano_planimetria"></div></div><div class="workspace"><div data-id=' + idPlitems + ' class="planimetria"><img class="img_planimetria"></div></div><div class="toolbar"><div class="toolbar_element_content dragscroll"><div class="toolbar_element toolbar_resize"><svg id="resize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>resize</title><path class="cls-1" d="M0,0V512H512V0ZM146,423.25v30H58.3V365.58h30v36.2l84.48-84.16L194,338.87l-84.69,84.38Zm26.81-228.86L88.3,110.22v36.2h-30V58.75H146v30H109.26L194,173.13ZM453.7,453.25H366v-30h36.71L318,338.87l21.18-21.25,84.48,84.16v-36.2h30Zm0-306.82h-30v-36.2l-84.48,84.16L318,173.13l84.69-84.38H366v-30H453.7Z"/></svg></div><div class="toolbar_element toolbar_elimina"><svg id="delete" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 459 459"><title>delete</title><path class="cls-1" id="trash" data-name="trash" d="M76.5,408a51.15,51.15,0,0,0,51,51h204a51.15,51.15,0,0,0,51-51V102H76.5ZM408,25.5H318.75L293.25,0H165.75l-25.5,25.5H51v51H408Z"/></svg></div><div class="toolbar_element toolbar_id_label"><svg id="label" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>label</title><path class="cls-1" d="M452,45H60C26.92,45,0,86.67,0,137.89V370.11C0,421.33,26.92,463,60,463H452c33.08,0,60-41.67,60-92.89V137.89C512,86.67,485.08,45,452,45ZM347,323.67H165V277.22H347Zm75-92.89H90V184.33H422Z"/></svg></div><div class="toolbar_separator"></div>' + await mascheraFiltri() + '</div></div><div class="select_items"><div class="arrow arrow_left"><svg id="arrow_l" class="arrow_l" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><defs><style>#arrow_l .cls-1{fill:none;stroke:#464646;stroke-miterlimit:10;stroke-width:350px;}</style></defs><title>arrow</title><polyline class="cls-1" points="999.1 1900.22 99.1 1000.22 99.81 999.56 999.81 99.56"/></svg></div><div class="items"><table class="items_table dragscroll"><tr></tr></table></div><div class="arrow arrow_rigth"><svg id="arrow_r" class="arrow_r" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><defs><style>#arrow_r .cls-1{fill:none;stroke:#464646;stroke-miterlimit:10;stroke-width:350px;}</style></defs><title>arrow</title><polyline class="cls-1" points="1000.81 1900.22 1900.81 1000.22 1900.1 999.56 1000.1 99.56"/></svg></div></div><div class="item_new_element"></div>';

	} else {

		content_html = '<div class="header"><svg class="go_back_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 408 408"><style>.freccia{fill: #464646;}</style><title>go_back</title><path class="freccia" d="M408,178.5H96.9L239.7,35.7,204,0,0,204,204,408l35.7-35.7L96.9,229.5H408Z"/></svg><svg class="save_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.save{fill:#464646;}</style></defs><title>save</title><rect class="save" x="287.35" y="52.24" width="62.69" height="104.49"/><path class="save" d="M417.06,0H5.22V512H506.78V89.71ZM109.71,20.9H381.39V188.08H109.71Zm303,470.2H88.82V271.67H412.73Z"/></svg><div class="piano_planimetria"></div><div class="zoom"><input type="range" min="10" max="250" value="100" class="slider"><div class="zoom_percentuale"><input value="100" type="number" min="10" max="250" class="zoom_percentuale_cifra">%</div></div></div><div class="workspace"><div class="planimetria" data-id=' + idPlitems + '><img class="img_planimetria"></div></div><div class="toolbar"><div class="toolbar_element_content dragscroll"><div class="toolbar_element toolbar_resize"><svg id="resize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>resize</title><path class="cls-1" d="M0,0V512H512V0ZM146,423.25v30H58.3V365.58h30v36.2l84.48-84.16L194,338.87l-84.69,84.38Zm26.81-228.86L88.3,110.22v36.2h-30V58.75H146v30H109.26L194,173.13ZM453.7,453.25H366v-30h36.71L318,338.87l21.18-21.25,84.48,84.16v-36.2h30Zm0-306.82h-30v-36.2l-84.48,84.16L318,173.13l84.69-84.38H366v-30H453.7Z"/></svg></div><div class="toolbar_element toolbar_elimina toolbar_elimina_desktop"><svg id="delete" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 459 459"><title>delete</title><path class="cls-1" id="trash" data-name="trash" d="M76.5,408a51.15,51.15,0,0,0,51,51h204a51.15,51.15,0,0,0,51-51V102H76.5ZM408,25.5H318.75L293.25,0H165.75l-25.5,25.5H51v51H408Z"/></svg></div><div class="toolbar_element toolbar_id_label"><svg id="label" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>label</title><path class="cls-1" d="M452,45H60C26.92,45,0,86.67,0,137.89V370.11C0,421.33,26.92,463,60,463H452c33.08,0,60-41.67,60-92.89V137.89C512,86.67,485.08,45,452,45ZM347,323.67H165V277.22H347Zm75-92.89H90V184.33H422Z"/></svg></div><div class="toolbar_separator"></div>' + await mascheraFiltri() + '</div></div><div class="select_items"><div class="arrow arrow_left"><svg id="arrow_l" class="arrow_l" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><defs><style>#arrow_l .cls-1{fill:none;stroke:#464646;stroke-miterlimit:10;stroke-width:350px;}</style></defs><title>arrow</title><polyline class="cls-1" points="999.1 1900.22 99.1 1000.22 99.81 999.56 999.81 99.56"/></svg></div><div class="items"><table class="items_table dragscroll"><tr></tr></table></div><div class="arrow arrow_rigth"><svg id="arrow_r" class="arrow_r" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><defs><style>#arrow_r .cls-1{fill:none;stroke:#464646;stroke-miterlimit:10;stroke-width:350px;}</style></defs><title>arrow</title><polyline class="cls-1" points="1000.81 1900.22 1900.81 1000.22 1900.1 999.56 1000.1 99.56"/></svg></div></div><div class="item_new_element"></div>';

	}

	var popup_html = '<div class="popup_background"><div class="popup_content"><div class="popup_menu"><table><tr><td><div class="popup_menu_element selected popup_menu_attributi">Attributi</div></td><td><div class="popup_menu_element popup_menu_checklist">Checklist</div></td></tr></table></div><div class="popup_contenuto"></div><div class="popup_option"><div class="popup_close">ANNULLA</div><div class="popup_apply">APPLICA</div></div></div></div>' + '<div class="popup_resize"><div class="popup_resize_title">Ridimensiona Items:</div><div class="resize_slider"><input type="range" name="resize_value" class="resize_value" min="1" max="8" value=""></div></div>';

	$('.content').addClass('modifica');
	$('.content').html(content_html);
	$('.popups').html(popup_html);

	console.log($('.planimetria').data()["id"]);
	//console.log(idPlitems);

	planimetria(await call("plitems_img_planimetria", idPlitems));
	riempiBarraElementi();
	caricaItems(idPlitems);
	preparaModifica();
	preparaStrumento();

}


async function riempiBarraElementi() {

	var classi_elemento = await call("classi_elemento");
	console.log(classi_elemento);
	var elements = JSON.parse(classi_elemento);
	console.log(elements);

	for (var i = 0; i < elements.length; i++) {
		if (onMobile()) {
			$(".items_table tr").append("<td><div class='item_element item_element_mobile elem_cat_" + elements[i]['fk_categoria'] + "' style='background-image: url(\"" + PATH_IMG_STR + elements[i]['src_img'] + "\")' data-elemento='" + elements[i]['nome_classe'] + "' data-id='" + elements[i]['id'] + "' data-cat='" + elements[i]['fk_categoria'] + "'></div></td>");
		} else {
			$(".items_table tr").append("<td><div class='item_element elem_cat_" + elements[i]['fk_categoria'] + "' style='background-image: url(\"" + PATH_IMG_STR + elements[i]['src_img'] + "\")' data-elemento='" + elements[i]['nome_classe'] + "' data-id='" + elements[i]['id'] + "' data-cat='" + elements[i]['fk_categoria'] + "'></div></td>");
		}
	}

}

async function salvaPlitem() {
	/*
	 *	procedura che crea un nuovo plitem, azzera quello vecchio e inserisce gli elementi (elements) nel plitem appena creato
	 *	passo indice plitem attuale che verrà elaborato per eliminare plitem attuale e restituire id plitem nuovo
	 * 	in seguito lato server verranno salvati tutti gli elementi
	 */

	//metto codice elemento = 000 dove non è stato inserito
	for (var i = 0; i < elements.length; i++) {
		if (elements[i] != null) {
			if (elements.cod == "null" || elements.cod == "") {
				elements.cod = "000";
			}
		}
	}

	var actualPlitemId = $(".planimetria").data()['id'];
	var new_zoom = $(".resize_value").val();

	var arr = {
		actual_id: actualPlitemId,
		zoom_perc: new_zoom,
		items: elements.filter(function(el) {
			return (el != null);

		}) //filter serve per copiare l'array togliendo i null
	};


	/*
		arr["actual_id"] = actualPlitemId;
		arr["zoom_perc"] = new_zoom;
	*/


	console.log(arr);

	zoom_perc = 3.5;

	var esito = await insert("insertPlitem", JSON.stringify(arr));
	console.log("Test", esito);
	return esito;


	/*@deprecated
		for(var i = 0; i < elements.length; i++){
			if(elements[i] != null){
				if(elements[i].cod == null || elements[i].cod == ""){
					elements[i].cod = "000";
				}
				salvaItem(elements[i], newPlitemId);
			}
		}
	*/


}


//@deprecated
async function salvaItem(item, newPlitemId) {
	console.log(newPlitemId);
	// qui creo vettore con dati che dovro mettere nella nuova tupla del db
	// posX, posY, fk_plitems, fk_elemento VALORI
	var newItem = new Array();
	newItem[0] = item.x;
	newItem[1] = item.y;
	newItem[2] = newPlitemId;
	newItem[3] = item.elemento;
	console.log(item.checklist);
	newItem[4] = JSON.stringify(item.attributi);
	newItem[5] = item.categoriaElemento;
	newItem[6] = JSON.stringify(item.checklist);
	newItem[7] = item.id;
	newItem[8] = item.cod;

	console.log("passo questo: ", await insert("updateItem", JSON.stringify(newItem)));

}
