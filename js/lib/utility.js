function getLayerX(e) {
	if (e.clientX === undefined) {
		var touch = e.touches[0];
		return touch.pageX;
	} else {
		return e.clientX;
	}
}

function getLayerY(e) {
	if (e.clientY === undefined) {
		var touch = e.touches[0];
		return touch.pageY;
	} else {
		return e.clientY;
	}
}

function onMobile(){
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		return true;
	}
	return false;
}

function closeClickOutside(e, class_box, class_content, arr = []) {
	offset_left= $(class_content).offset().left;
	offset_top= $(class_content).offset().top;
	width= $(class_content).innerWidth();
	height= $(class_content).innerHeight();

	var eventi = 'click touch mousedown touchstart mouseup touchend mousemove touchmove mouseleave dblclick contextmenu';
	if (((getLayerX(e)<offset_left) || (getLayerX(e)>offset_left+width)) || ((getLayerY(e)<offset_top) || (getLayerY(e)>offset_top+height))) {
		$(class_box).css({'display' : 'none'});
		console.log("Chiudo");
		$('body').off(eventi, class_content);
		$('body').off(eventi, class_box);

		for(var i=0; i<arr.length; i++) {
			$('body').off(eventi, arr[i]);
		}
	}
}