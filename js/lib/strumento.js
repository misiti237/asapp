TENTATIVI_MASSIMI = 2000

var contatore = 0
function caricaPlanimetria(src) {
	$('.img_planimetria').attr('src', src);

	function isLoad() {
		contatore += 1
		if (contatore >= TENTATIVI_MASSIMI) {
			clearInterval(checkCiclo);
	    checkCiclo = null;
		}
	if ($('img.img_planimetria')[0].naturalWidth == 0) {
	  return false;
	}

	clearInterval(checkCiclo);
	checkCiclo = null;

	var plan_width = $('.img_planimetria')[0].naturalWidth;
	var plan_height = $('.img_planimetria')[0].naturalHeight;
	if (plan_width >= plan_height) {
	  $('.planimetria').css({
	    'height': '99%',
	    'width': 'auto'
	  });
	  $('.img_planimetria').css({
	    'width': 'auto',
	    'height': '99%'
	  });
	} else {
	  $('.planimetria').css({
	    'width': '99%',
	    'height': 'auto'
	  });
	  $('.img_planimetria').css({
	    'width': '99%',
	    'height': 'auto'
	  });
	}
	}

	var checkCiclo = setInterval(isLoad, 10);
}

function planimetria(src) {
	caricaPlanimetria(src);
	$('.zoom_percentuale_cifra').val(100);
	$('.slider').val(100);
	if (onMobile()) {
		$('.workspace').css({
			'overflow': 'auto'
		});
	} else {
		$('.planimetria').addClass('dragscroll');
	}
}

function aggiornaFreccieItem() { //AGGIORNAMENTO
  if ($('.items_table').position().left < 0) {
    $('.arrow_left').css({
      'display': 'block'
    });
  } else {
    $('.arrow_left').css({
      'display': 'none'
    });
  }


  if ((($('.items_table').innerWidth() - ($('.items').innerWidth() + 20)) + $('.items_table').position().left) <= 0) {
    $('.arrow_rigth').css({
      'display': 'none'
    });
  } else {
    $('.arrow_rigth').css({
      'display': 'block'
    });
  }
}

var first_pos = null;
var off = null;

var drag_check = false;

function preparaStrumento() { //ELEMENTI COMUNI TRA MODIFICA E VERIFICA

  //FILTRI
  function refreshFiltri() {
    $('.toolbar_layer').each(function() {
      var categoria = $(this).attr('data-id');
      if ($(this).hasClass('layer_disabled')) {
        $('.elem_cat_' + categoria).css({
          'display': 'none'
        });
      } else {
        $('.elem_cat_' + categoria).css({
          'display': ''
        });
      }
    });
  }

  $('body').on('click', '.toolbar_layer', function() {
    $('.items_table').css({
      'left': '0px'
    });
    $('.arrow_left').css({
      'display': 'none'
    });
    $(this).toggleClass('layer_disabled');
    refreshFiltri();
  });

  // TOGGLE LABEL --- fff299
  var check_toggle_label = false;
  $('body').on('click', '.toolbar_id_label', function() {
    if (!check_toggle_label) {
      $('.id_label').css({
        'display': 'block'
      });
      $('.toolbar_id_label svg .cls-1').css({
        'fill': '#fff299'
      });
    } else {
      $('.id_label').css({
        'display': ''
      });
      $('.toolbar_id_label svg .cls-1').css({
        'fill': ''
      });
    }
    check_toggle_label = !check_toggle_label;
  });

  //GESTIONE ZOOM INIZIO ---
  var zoom_perc = 100;

  function zoom() {
    var plan_width = $('.img_planimetria').innerWidth();
    var plan_height = $('.img_planimetria').innerHeight();
    if (plan_width >= plan_height) {
      $('.planimetria').css({
        'height': zoom_perc + '%'
      });
      $('.img_planimetria').css({
        'width': 'auto',
        'height': '100%'
      });
    } else {
      $('.planimetria').css({
        'width': zoom_perc + '%'
      });
      $('.img_planimetria').css({
        'width': '100%',
        'height': 'auto'
      });
    }

    //ADATTAMENTO ZOOM ASSE X
    if (onMobile()) {
      $('.planimetria').css({
        'left': '0px'
      });
    } else {
      if (($('.planimetria').innerWidth() + $('.planimetria').position().left) < $('.workspace').innerWidth()) {
        $('.planimetria').css({
          'left': -($('.planimetria').innerWidth() - $('.workspace').innerWidth()) + 'px'
        });
      }
      if (($('.planimetria').innerWidth()) < $('.workspace').innerWidth()) {
        $('.planimetria').css({
          'left': '0px'
        });
      }
    }
    //ADATTAMENTO ZOOM ASSE Y
    if (onMobile()) {
      $('.planimetria').css({
        'left': '0px'
      });
    } else {
      if (($('.planimetria').innerHeight() + $('.planimetria').position().top) < $('.workspace').innerHeight()) {
        $('.planimetria').css({
          'top': -($('.planimetria').innerHeight() - $('.workspace').innerHeight()) + 'px'
        });
      }
      if (($('.planimetria').innerHeight()) < $('.workspace').innerHeight()) {
        $('.planimetria').css({
          'top': '0px'
        });
      }
    }
  }


  function refreshZoom(what) {
    if (what == 1) {
      zoom_perc = Number($('.zoom_percentuale_cifra').val());
      $('.slider').val(zoom_perc);
    }
    if (what == 2) {
      zoom_perc = Number($('.slider').val());
      $('.zoom_percentuale_cifra').val(zoom_perc);
    }
    zoom();
  }

  function refreshZoomDesktop() {
    $('.slider').val(zoom_perc);
    $('.zoom_percentuale_cifra').val(zoom_perc);
  }
  $('body').on('change blur', '.zoom_percentuale_cifra', function() {
    var val = Number($(this).val());
    if (val < 10) {
      $(this).val(10);
    }
    if (val > 250) {
      $(this).val(250);
    }
    refreshZoom(1);
  });

  $('body').on('change input blur', '.slider', function() {
    refreshZoom(2);
  });
  //GESTIONE ZOOM FINE ---

  //ZOOM MOBILE INIZIO ---
  if (onMobile()) {
    //boolean per gestire lo zoom
    var firstTouch = true;
    var initScaleX;
    var initScaleY;
    var init_perc = 0;
    $('body').on('touchstart', '.workspace', function(e) { //ABBIAMO MESSO IL MOUSEDOWN NONOSTANTE PRIMA FUNZIONASSE SOLO CON IL MOUSEMOVE .suca.cazzituoi.
      $('body').on('touchmove', '.workspace', function(e) {
        //console.log(e);
        if (e.touches.length == 2) {
          //Funzione per il pinch to zoom
          //Coordinate per dito 1 e 2 del tocco
          var xpos1 = e.touches[0].pageX;
          var ypos1 = e.touches[0].pageY;
          var xpos2 = e.touches[1].pageX;
          var ypos2 = e.touches[1].pageY;

          //init variabili di supporto
          var zoomingX = 0;
          var zoomingY = 0;
          var ScaleX = 0;
          var ScaleY = 0;

          //Controllo qual'è il dito con la coordinata x e y maggiore
          if (xpos1 > xpos2) {
            //differenza tra le x delle due dita
            ScaleX = xpos1 - xpos2;
          } else {
            //differenza tra le x delle due dita
            ScaleX = xpos2 - xpos1;
          }
          if (ypos1 > ypos2) {
            //differenza tra le y delle due dita
            ScaleY = ypos1 - ypos2;
          } else {
            //differenza tra le y delle due dita
            ScaleY = ypos2 - ypos1;
          }
          if (firstTouch) {
            //Controllo se sono al primo ciclo di touch con 2 dita, per impostare le coordinate iniziali
            initScaleX = ScaleX;
            initScaleY = ScaleY;
            //Boolean per confermare l'avvenimento del primo tocco
            firstTouch = false;
            init_perc = zoom_perc;
          } else {
            //Calcolo quantitativo di zoom, posizione delle dita - posizione iniziale delle dita
            zoomingX = ScaleX - initScaleX;
            zoomingY = ScaleY - initScaleY;
            //Creo la variabile con il quantitativo di zoom finale
            var ZOOM = zoomingX + zoomingY;
            //Elimino le cifre decimali
            ZOOM = Math.floor(ZOOM / 10);
            zoom_perc = init_perc + ZOOM;
            if (zoom_perc <= 10) {
              zoom_perc = 10;
            }
            if (zoom_perc >= 250) {
              zoom_perc = 250;
            }
            //Imposto lo zoom dell'elemento e scrivo nel popup quanto ho zoommato
            zoom();
          }
          refreshZoomDesktop();
        }
      });
    });
    $('body').on('mouseup touchend', '.workspace', function(e) {
      //quando stacco le dita dal display resetto tutte le variabili, tra cui quella riguardante il primo touch
      e.touches = [];
      firstTouch = true;
      initScaleX = null;
      initScaleY = null;
      ScaleX = null;
      ScaleY = null;
      $('body').off('mousemove touchmove', '.workspace');

      //$('.planimetria').css({'overflow' : 'auto'});
      //salvo in una variabile la dimensione finale dell'element zoomato
    });
  }
  //ZOOM MOBILE FINE ---

  //SCRIPT GRAFICA INIZIO ---

  $('body').on('click touch', '.popup_menu_element', function() {
    $('.popup_menu_element').removeClass('selected');
    $(this).addClass('selected');
  });
  //SCRIPT GRAFICA FINE ---

  //DRAGSCROLL INIZIO ---
  $('body').on('mousedown touchstart', '.dragscroll', function(e) {
    e.preventDefault();
    if (!drag_check) {
      if (first_pos == null) {
        first_pos = new Array();
        first_pos['x'] = getLayerX(e);
        first_pos['y'] = getLayerY(e);

        off = new Array();
        off['x'] = $(this).position().left;
        off['y'] = $(this).position().top;
      }


      $('body').on('mousemove touchmove', '.dragscroll', function(e) {
        if (list_page['modifica']) { //ABBIAMO MESSO L'IF MA C'ERA UN TRY-CATCH CHE FUNZIONAVA .suca.cazzituoi.
          aggiornaFreccieItem();
        }
        if ($(this).innerWidth() > $(this).parent().innerWidth()) { //ASSE X
          e.preventDefault();

          calcolo = ((first_pos['x'] - getLayerX(e)) * (-1)) + off['x'];
          if (calcolo > 0) {
            $(this).css({
              'left': '0px'
            });
          } else {
            confronto = $(this).innerWidth() * (-1) + $(this).parent().innerWidth();
            if (($(this).position().left <= confronto) && ((first_pos['x'] - getLayerX(e)) >= 0)) {} else {
              $(this).css({
                'left': calcolo + 'px'
              });
            }
          }
        }

        if ($(this).innerHeight() > $(this).parent().innerHeight()) { //ASSE Y
          e.preventDefault();

          calcolo = ((first_pos['y'] - getLayerY(e)) * (-1)) + off['y'];
          if (calcolo > 0) {
            $(this).css({
              'top': '0px'
            });
          } else {
            confronto = $(this).innerHeight() * (-1) + $(this).parent().innerHeight();
            if (($(this).position().top <= confronto) && ((first_pos['y'] - getLayerY(e)) >= 0)) {} else {
              $(this).css({
                'top': calcolo + 'px'
              });
            }
          }
        }

      });
    }
  });
  $('body').on('mouseup touchend', '.dragscroll', function(e) {
    $(this).css({
      'cursor': ''
    });
    $('body').off('mousemove touchmove', '.dragscroll');
    first_pos = null;
    off = null;
  });
  $('body').on('mouseleave', '.dragscroll', function(e) {
    $('body').off('mousemove touchmove', '.dragscroll');
    first_pos = null;
    off = null;
  });
  //DRAGSCROLL FINE ---
}

function aggiornaBadge(i) {
  switch (elements[i].checkValidita()) {
    case 0:
      $('.el_' + i + ' .element_badge').css({
        'background-color': 'red'
      });
      break;
    case 1:
      $('.el_' + i + ' .element_badge').css({
        'background-color': 'red'
      });
      break;
    case 2:
      $('.el_' + i + ' .element_badge').css({
        'background-color': 'yellow'
      });
      break;
    case 3:
      $('.el_' + i + ' .element_badge').css({
        'background-color': 'green'
      });
      break;
  }
}
var zoom_perc = 3.5;

async function caricaItems(idPlitems) {
  var itemsFromServer = JSON.parse(await call("items", idPlitems));
  console.log(itemsFromServer);

  zoom_perc = await call('zoom_perc', idPlitems);

  $(".resize_value").val(zoom_perc);

  for (var i = 0; i < itemsFromServer.length; i++) {
    var item = itemsFromServer[i];
    elements.push(new Element(i, item['posX'], item['posY'], PATH_IMG_STR + item['src_img'], item['fk_plitems'], item['fk_elemento'], item['attributi'], item['checklist'], null, item['codice'], item['id'], zoom_perc, item['categoria']));
	console.log(elements);
	//++elements.length;
    //$('.el_'+i+' .element_badge').()
    aggiornaBadge(i);
  }

}

async function getCategorie() {
  var categorie = JSON.parse(await call('categorie_elemento'));

  return categorie;
}


//variabile per mostrare attributi e checklist di elemento selezionato
var selected_item = null;

async function visualizzaAttributi(id) {

  selected_item = id;

  var attributi = elements[id]['attributi'];
  var codice = elements[id]['cod'];

  $(".popup_contenuto").empty();
  //$(".popup_contenuto").append("<table class='attributi'><tr><td>Dati</td><td>Valore</td><td>Descrizione</td></tr></table>");



  for (var i = 0; i < attributi.length; i++) {
    var attr = attributi[i];
    //$(".attributi").append("<tr><td>"+attr['nome']+"</td><td><input id=a_"+i+" type = 'text' ></td><td>"+attr['descrizione']+"</td></tr>");
    //$("#a_"+i).val(attr["valore"]);
    //se sei in verifica non devi mostrare il campo note tra gli attributi ma in checklist
    if (!((i == 10) && list_page['verifica'])) {
      $(".popup_contenuto").append('<div class="popup_input_box attributo"><div class="popup_input_label">' + attr['nome'] + '</div><input id=a_' + i + ' class="popup_input" type="text" maxlength="50" value="' + attr["valore"] + '"><div class="popup_input_bottom"></div><div class="popup_input_descrizione">' + attr['descrizione'] + '</div></div>');
    }
  }
  //codice
  $(".popup_contenuto").prepend('<div class="popup_input_box attributo"><div class="popup_input_label">Codice</div><input id=a_' + i + ' class="popup_input popup_add_codice" type="text" maxlength="6" value="' + codice + '"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_codice_err"></div></div>');
  //data_scadenza
  $('#a_8').prop('type', 'date');

  if (list_page['modifica']) $('.attributo input').prop('readonly', false);
  if (list_page['verifica']) {
    //in verifica solo campo note deve essere modificabile
    $('.attributo input').prop('readonly', true);
    $('#a_10').prop('readonly', false);
  }

}

async function visualizzaChecklist(id) {
  selected_item = id;

  var checklist = elements[id]['checklist'];

  $(".popup_contenuto").empty();

  if (checklist == null) {
    var idClasseElemento = elements[id]['categoriaElemento'];
    checklist = JSON.parse(await call("items_checklist", idClasseElemento));
  }

  if (list_page['modifica']) {
    for (var i = 0; i < checklist.length; i++) {
      var check = checklist[i];
      $(".popup_contenuto").append('<div class="checklist_element"><table><tr><td class="check_nome">' + check['nome'] + ' <span style="color: #027BD3">*</span></td><td class="check_ok"></td><td class="check_ko"></td><td class="check_na"></td></tr></table></div>');
    }
  }

  if (list_page['verifica']) {
    //$(".popup_contenuto").append("<table class='checklist'><tr><td>Check</td><td>Descrizione</td><td>Obbligatorieta</td><td>Ok</td><td>Ko</td><td>NA</td></tr></table>");
    for (var i = 0; i < checklist.length; i++) {
      var check = checklist[i];
      if (check['obbligatorieta'] == '1') {
        //$(".checklist").append("<tr><td>"+check['nome']+"</td><td>"+check['descrizione']+"</td><td>"+check['obbligatorieta']+"</td><td><input type='radio' name='check"+i+"' value='0' '></td><td><input type='radio' name='check"+i+"' value='1' '></td></tr>");
        $(".popup_contenuto").append('<div class="checklist_element"><table><tr><td class="check_nome">' + check['nome'] + ' <span style="color: #027BD3">*</span></td><td class="check_ok"><input type="radio" name="check' + i + '" value="0"></td><td class="check_ko"><input type="radio" name="check' + i + '" value="1"></td><td class="check_na"></td></tr></table></div>');
      } else {
        //$(".checklist").append("<tr><td>"+check['nome']+"</td><td>"+check['descrizione']+"</td><td>"+check['obbligatorieta']+"</td><td><input type='radio' name='check"+i+"' value='0' '></td><td><input type='radio' name='check"+i+"' value='1' '></td><td><input type='radio' name='check"+i+"' value='2''></td></tr>");
        $(".popup_contenuto").append('<div class="checklist_element"><table><tr><td class="check_nome">' + check['nome'] + '</td><td class="check_ok"><input type="radio" name="check' + i + '" value="0"></td><td class="check_ko"><input type="radio" name="check' + i + '" value="1"></td><td class="check_na"><input type="radio" name="check' + i + '" value="2"></td></tr></table></div>');
      }
      if (check['valore'] != '') $("input[name='check" + i + "'][value=" + check['valore'] + "]").prop('checked', true);
    }

    try {
      var note = elements[id]['attributi'][10];
      console.log(elements[id]['attributi']);
      $(".popup_contenuto").append('<div class="popup_input_box attributo"><input id=a_' + 10 + ' class="popup_input" type="text" maxlength="50" value="' + note["valore"] + '"><div class="popup_input_bottom"></div><div class="popup_input_descrizione">' + note['descrizione'] + '</div></div>');
    } catch (e) {

    }
  }
}

$("body").on("click touch", ".save_button", async function() {
  if (list_page['modifica']) {
    var esito = await salvaPlitem();
    $('.content').html('');
    offEventStrumento();
    $('.content').attr('class', 'content');
    indirizzaModifica();
  }
});

$("body").on("click", ".popup_menu_element", function() {
  if ($(this).hasClass('popup_menu_attributi')) visualizzaAttributi(selected_item);
  if ($(this).hasClass('popup_menu_checklist')) visualizzaChecklist(selected_item);

});

$('body').on('dblclick', '.item_rappr', function() { //ABBIAMO TOLTO IL TOUCH ANCHE SE COMUNQUE TUTTO FUNZIONAVA .suca.cazzituoi.
  console.log('APRI CHECKLIST');
  $('.selected').removeClass('selected');
  if (list_page['modifica']) {
    $('.popup_menu_attributi').addClass('selected');
    visualizzaAttributi($(this).data("id"));
  }

  if (list_page['verifica']) {
    $('.popup_menu_checklist').addClass('selected');
    visualizzaChecklist($(this).data("id"));
  }

  $('.popup_background').css({
    'display': 'block'
  });

  $('body').on('click', '.popup_background', function(e) {
    closeClickOutside(e, '.popup_background', '.popup_content');
  });


});
