function indirizzaVerifica(){
  mascheraEdifici();
  elements.length = 0;
}

async function apriVerifica(idPlitems) {

	var content_html;

	if(onMobile()) {

		content_html= '<div class="header"><svg class="go_back_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 408 408"><style>.freccia{fill: #464646;}</style><title>go_back</title><path class="freccia" d="M408,178.5H96.9L239.7,35.7,204,0,0,204,204,408l35.7-35.7L96.9,229.5H408Z"/></svg><div class="piano_planimetria">Primo Piano</div></div><div class="workspace"><div data-id='+idPlitems+' class="planimetria"><img class="img_planimetria"></div></div>' + /*TOOLBAR*/ '<div class="toolbar"><div class="toolbar_element_content dragscroll"><div class="toolbar_element toolbar_id_label"><svg id="label" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>label</title><path class="cls-1" d="M452,45H60C26.92,45,0,86.67,0,137.89V370.11C0,421.33,26.92,463,60,463H452c33.08,0,60-41.67,60-92.89V137.89C512,86.67,485.08,45,452,45ZM347,323.67H165V277.22H347Zm75-92.89H90V184.33H422Z"/></svg></div><div class="toolbar_separator"></div>' + await mascheraFiltri() + '</div></div>';

	} else {

		content_html= '<div class="header"><svg class="go_back_button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 408 408"><style>.freccia{fill: #464646;}</style><title>go_back</title><path class="freccia" d="M408,178.5H96.9L239.7,35.7,204,0,0,204,204,408l35.7-35.7L96.9,229.5H408Z"/></svg><div class="piano_planimetria">Primo Piano</div><div class="zoom"><input type="range" min="10" max="250" value="100" class="slider"><div class="zoom_percentuale"><input value="100" type="number" min="10" max="250" class="zoom_percentuale_cifra">%</div></div></div><div class="workspace"><div data-id='+idPlitems+' class="planimetria"><img class="img_planimetria"></div></div>' + /*TOOLBAR*/ '<div class="toolbar"><div class="toolbar_element_content dragscroll"><div class="toolbar_element toolbar_id_label"><svg id="label" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#eee;}</style></defs><title>label</title><path class="cls-1" d="M452,45H60C26.92,45,0,86.67,0,137.89V370.11C0,421.33,26.92,463,60,463H452c33.08,0,60-41.67,60-92.89V137.89C512,86.67,485.08,45,452,45ZM347,323.67H165V277.22H347Zm75-92.89H90V184.33H422Z"/></svg></div><div class="toolbar_separator"></div>' + await mascheraFiltri() + '</div></div>';

	}



  var popup_html= '<div class="popup_background"><div class="popup_content"><div class="popup_menu"><table><tr><td><div class="popup_menu_element selected popup_menu_attributi">Attributi</div></td><td><div class="popup_menu_element popup_menu_checklist">Checklist</div></td></tr></table></div><div class="popup_contenuto"></div><div class="popup_option"><div class="popup_close">ANNULLA</div><div class="popup_apply">APPLICA</div></div></div>';



	$('.content').addClass('verifica');

	$('.content').html(content_html);

	$('.popups').html(popup_html);


	
	planimetria(await call("plitems_img_planimetria", idPlitems));

  	caricaItems(idPlitems);
	
	preparaStrumento();

}



function salvaVerifica(){

  //controllo validità

  for(var i = 0; i < elements.length; i++){

    var el = elements[i];

    for(var j = 0; j < el.checklist.length; j++){

      var check = el.checklist[j];

      if(check.obbligatorieta == 1 && (check.valore == null || check.valore == "")) return false;

    }

  }



  for(var i = 0; i < elements.length; i++){

    var el = elements[i];

		if(el != null) salvaVerificaItem(el.id, el.checklist)

	}

  return true;

}



async function salvaVerificaItem(item){

  console.log("cristo", item.checklist);
  await insert('nuovaVerifica', item.id);

  for(var i = 0; i < item.checklist.length; i++){

    var check = item.checklist[i];
    var verifica = new Array();
    verifica[0] = check.valore;
    verifica[1] = check.id;
    verifica[2] = item.id;

    console.log("ok: ",await insert('verifica', JSON.stringify(verifica)));

  }

}
