function adatta_edifici() {
	if (edifici.length*250 >= ($(window).innerWidth()*95/100)) {
		var spazio= Math.floor($(window).innerWidth()*95/100/250);
		$('.edifici').css({'width' : (spazio*250)+'px'});
		$('.edifici').css({'text-align' : 'left'});
	} else {
		$('.edifici').css({'width' : ''});
		$('.edifici').css({'text-align' : ''});
	}

	if ( ($('.edifici').innerHeight()+50) <= $(window).innerHeight() ) {
		$('.edifici').css({'top' : ( ($(window).innerHeight() - $('.edifici').innerHeight())/2 )+'px'});
	} else {
		$('.edifici').css({'top' : '0'});
	}
}

async function caricaEdifici(){

  var edificiFromServer = JSON.parse(await call("edifici"));
	for(var i = 0; i < edificiFromServer.length; i++){
		edificiFromServer[i]['piani'] = JSON.parse(await call("piani_edificio", edificiFromServer[i].id));
	}
  //var piani = JSON.parse(await call("piani_edificio", edificiFromServer[0].id));
	return edificiFromServer;
}

async function mascheraEdifici() {
	// if(edifici == null){
	// 	edifici = await caricaEdifici();
	// }
    $('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
	edifici = await caricaEdifici();
    console.log("MEH HO FINITO");

	loadBar(70);

	var html='<div class="edifici">';

	for (var i=0; i<edifici.length; i++) {
		var edificio_nome= '<div class="edificio_titolo">'+edifici[i]['nome']+'</div>';

		var edificio= '<div class="edificio"  data-index="'+i+'"><div class="edificio_icona" style="background-image:url(images/edificio.jpg)"></div>'+edificio_nome+'</div>';
		html+= edificio;
	}
	if(list_page['modifica']) html+="</div><div class='add_edificio'>AGGIUNGI EDIFICIO</div>";
	var popups='<div class="popup_edificio_background"><div class="popup_edificio"><div class="popup_edificio_nome"></div><div class="popup_edificio_content"></div><div class="popup_edificio_option"><div class="popup_edificio_close"></div><div class="popup_edificio_add"></div></div></div></div><div class="delete_edificio_icon">' + '<svg id="delete_edificio" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><defs><style>#delete_edificio .cls-1{fill:#951515;}</style></defs><title>delete_edificio</title><g id="delete_edificio_circle"><path class="cls-1" d="M1000,105a902.34,902.34,0,0,1,180.42,18.18,890.21,890.21,0,0,1,320,134.65,897.79,897.79,0,0,1,324.3,393.81,890.39,890.39,0,0,1,52.14,167.94,904.34,904.34,0,0,1,0,360.84,890.21,890.21,0,0,1-134.65,320,897.65,897.65,0,0,1-393.81,324.3,890.39,890.39,0,0,1-167.94,52.14,904.34,904.34,0,0,1-360.84,0,890.06,890.06,0,0,1-320-134.65,897.59,897.59,0,0,1-324.31-393.81,890.39,890.39,0,0,1-52.14-167.94,904.34,904.34,0,0,1,0-360.84,890.06,890.06,0,0,1,134.65-320A897.74,897.74,0,0,1,651.64,175.32a890.39,890.39,0,0,1,167.94-52.14A902.34,902.34,0,0,1,1000,105m0-40C483.61,65,65,483.61,65,1000s418.61,935,935,935,935-418.61,935-935S1516.39,65,1000,65Z"/></g><path id="delete_edificio_trash" class="cls-1" d="M709.39,1339.22c0,53.23,43.55,96.78,96.78,96.78h387.11c53.23,0,96.78-43.55,96.78-96.78V758.56H709.39Zm629.05-725.83H1169.08L1120.69,565H878.75l-48.39,48.39H661v96.78h677.44Z"/></svg>' + '</div>';

	$('.content').html(html);
	$('.popups').html(popups);

	if (onMobile()) {
		$('.edificio_icona').addClass('no_selectable');
	}

	adatta_edifici();
	loadBar(100);

}

function caricaPopupAddEdificio(aggiunta, id_edificio = null) {
    if (aggiunta) {
		$('.popup_edificio_nome').html('Aggiungi Edificio');
	} else {
		$('.popup_edificio_nome').html('Dati Edificio');
	}
	popup_content = '<div class="popup_input_box"><div class="popup_input_label">Nome</div><input class="popup_input popup_add_edificio_nome" type="text" maxlength="50"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_edificio_nome_err"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Codice MIUR</div><input class="popup_input popup_add_edificio_miur" type="text" maxlength="10"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_edificio_miur_err"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Indirizzo</div><input class="popup_input popup_add_edificio_indirizzo" type="text" maxlength="255"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_edificio_indirizzo_err"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Civico</div><input class="popup_input popup_add_edificio_civico" type="text" maxlength="5"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_edificio_civico_err"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">CAP</div><input class="popup_input popup_add_edificio_cap" type="tel" maxlength="5"><div class="popup_input_bottom"></div><div class="popup_input_err popup_add_edificio_cap_err"></div></div>';
	$('.popup_edificio_close').html('CHIUDI');
    if (aggiunta) {
		$('.popup_edificio_add').html('AGGIUNGI');
	} else {
		$('.popup_edificio_add').html('SALVA');
	}
		$('.popup_edificio_content').html(popup_content);

	if (list_page['verifica']) {
		$('.popup_edificio_add').remove();
		$('.popup_input').attr('readonly', 'true');
	}

    $('body').on('click touch', '.popup_edificio_background', function(e) {
		closeClickOutside(e, '.popup_edificio_background', '.popup_edificio');
	});

	$('.popup_edificio_background').css({'display' : 'block'});

    $('body').off('click touch', '.popup_edificio_close');
    $('body').off('click touch', '.popup_edificio_add');

    $('body').on('click touch', '.popup_edificio_close', function() {
        $('body').off('click touch', '.popup_edificio_close');
        $('body').off('click touch', '.popup_edificio_add');

        $('.popup_edificio_background').css({'display' : 'none'});
    });
    $('body').on('click touch', '.popup_edificio_add', async function() {
		//metto off per evitare multiclick
		var nome = $(".popup_add_edificio_nome").val();
		var miur = $('.popup_add_edificio_miur').val();
		var indirizzo = $('.popup_add_edificio_indirizzo').val();
		var civico = $('.popup_add_edificio_civico').val();
		var cap = $('.popup_add_edificio_cap').val();

		$('.popup_add_edificio_nome_err').html('');
		$('.popup_add_edificio_miur_err').html('');
		$('.popup_add_edificio_indirizzo_err').html('');
		$('.popup_add_edificio_civico_err').html('');
		$('.popup_add_edificio_cap_err').html('');
		/*
		if(nome != "" && nome != null){
            if(!edificioEsiste(nome)){
                $('body').off('click touch', '.popup_edificio_add');
                console.log("APPOSTO");
                console.log(nome);
    			var esito = await insert("addEdificio", nome);
    			console.log(nome);
    			console.log(esito);
    			if(esito){
    				indirizzaModifica();
    			}else{
    				console.log("errore");
    			}
            }else{
                $('.popup_add_edificio_nome_err').html('Edificio già esistente.');
                console.log("nome esiste gia");
            }
		}else{
            $('.popup_add_edificio_nome_err').html('Inserire nome edificio.');
            console.log("nome vuoto");
        }
		*/

		var checkValidita = true;

		if (nome == "" || nome == null) {
			$('.popup_add_edificio_nome_err').html('Inserire nome edificio.');
			checkValidita = false;
		} else {
			if (edificioEsiste(nome) && aggiunta) {
				$('.popup_add_edificio_nome_err').html('Edificio già esistente.');
				checkValidita = false;
			}
		}

		if (miur == "" || miur == null) {
			$('.popup_add_edificio_miur_err').html('Inserire codice MIUR.');
			checkValidita = false;
		}

		if (indirizzo == "" || indirizzo == null) {
			$('.popup_add_edificio_indirizzo_err').html('Inserire indirizzo.');
			checkValidita = false;
		}

		if (civico == "" || civico == null) {
			$('.popup_add_edificio_civico_err').html('Inserire civico.');
			checkValidita = false;
		}

		if ( isNaN( Number(cap) ) ) {
			$('.popup_add_edificio_cap_err').html('Il CAP deve contenere solo numeri.');
			checkValidita = false;
		} else {
			if (cap.length != 5) {
				$('.popup_add_edificio_cap_err').html('Il CAP deve essere lungo 5 caratteri.');
				checkValidita = false;
			}
		}

		var edificio_arr = [];
			edificio_arr[0] = nome;
			edificio_arr[1] = miur;
			edificio_arr[2] = indirizzo;
			edificio_arr[3] = civico;
			edificio_arr[4] = cap;
			if (!aggiunta) {
				edificio_arr[5] = id_edificio;
			}


		if (checkValidita) {
			$('body').off('click touch', '.popup_edificio_add');
			console.log("APPOSTO");
			console.log(nome);
			$('.popup_edificio_content').html('<div class="loading_gif"><img src="images/loading_blue.gif"></div>');
			if(aggiunta){
				var esito = await insert("addEdificio", JSON.stringify(edificio_arr));
			}else{
				var esito = await insert("updateEdificio", JSON.stringify(edificio_arr));
			}

			console.log(nome);
			console.log(id_edificio);
			console.log(esito);
			if(esito){
				indirizzaModifica();
			}else{
				alert('Errore anomalo durante la creazione dell\'edificio');
			}
		}


    });
}
$('body').on('click touch', '.add_edificio', function() {
    caricaPopupAddEdificio(true);
});
$("body").on("click touch", ".popup_edificio_piano", async function(){
	//if (!onMobile()) {
		var idPiano = $(this).parent().data()["id"];
        var idEdificio = $(this).parent().data()["edificio"];
		var idPlitems = await call("plitems_id", idPiano);
        var piano;
//        for(var i = 0; i<edifici[idEdificio].piani.length; i++){
//            var current_piano = edifici[idEdificio].piani[i];
//            if(current_piano.id == idPiano){
//                piano = current_piano.piani[i];
//            }
//        }

		if(list_page['modifica']){
			apriModifica(idPlitems, piano);
		}
		if(list_page['verifica']){
			apriVerifica(idPlitems);
		}
	//}
});

function edificioEsiste(nomeEdificio){
    for(var i = 0; i < edifici.length; i++){
        console.log("che bello");
        if(edifici[i].nome.toUpperCase() == nomeEdificio.toUpperCase()){
            return true;
        }
    }
    return false;

}

function caricaPopupEdificio (index) {
	console.log("funziono");
	var edificio_nome = edifici[index]['nome'];
	var edificio_id = edifici[index]['id'];
	var piani = edifici[index]['piani']; //ARRAY

	$('.popup_edificio_nome').html(edificio_nome + ' <svg id="info" class="info_edificio" data-edificio = "'+index+'" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.63 23.63"><defs><style>.cls-1{fill:#030104;}</style></defs><title>info</title><path class="cls-1" d="M11.81,0A11.81,11.81,0,1,0,23.63,11.81,11.81,11.81,0,0,0,11.81,0Zm2.46,18.31q-.91.36-1.45.55a3.84,3.84,0,0,1-1.26.19,2.52,2.52,0,0,1-1.72-.54,1.74,1.74,0,0,1-.61-1.37,5,5,0,0,1,0-.66c0-.22.08-.48.15-.76L10.18,13c.07-.26.13-.5.17-.73a3.24,3.24,0,0,0,.07-.63,1,1,0,0,0-.21-.72,1.2,1.2,0,0,0-.81-.2,2.13,2.13,0,0,0-.61.09L8.26,11l.2-.83q.75-.3,1.43-.52a4.22,4.22,0,0,1,1.29-.22,2.47,2.47,0,0,1,1.69.53,1.76,1.76,0,0,1,.59,1.38q0,.18,0,.62a4.13,4.13,0,0,1-.15.81l-.76,2.68c-.06.22-.12.46-.17.74a3.89,3.89,0,0,0-.07.63.9.9,0,0,0,.24.73,1.34,1.34,0,0,0,.83.19,2.4,2.4,0,0,0,.63-.1,3.56,3.56,0,0,0,.51-.17ZM14.14,7.43a1.81,1.81,0,0,1-1.27.49,1.83,1.83,0,0,1-1.28-.49A1.57,1.57,0,0,1,11,6.24,1.59,1.59,0,0,1,11.58,5a1.81,1.81,0,0,1,1.28-.5,1.79,1.79,0,0,1,1.27.5,1.61,1.61,0,0,1,0,2.39Z"/></svg>');
	var popup_content = '';
	for(var i=0; i<piani.length; i++) {
		if (list_page['modifica']) {
			popup_content += '<div class="popup_edificio_piano_box" data-id = "'+piani[i]['id']+'" data-edificio = "'+edificio_id+'"><div  class="popup_edificio_piano">'+piani[i]['nome']+'</div><div class="popup_edificio_piano_menu"><svg id="context_menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>#context_menu .cls-1{fill:#464646;}</style></defs><title>context_menu</title><g id="context_menu_body"><rect class="cls-1" x="216.75" y="407" width="78.5" height="78.5"/><rect class="cls-1" x="217.75" y="217" width="78.5" height="78.5"/><rect class="cls-1" x="216.75" y="27" width="78.5" height="78.5"/></g></svg></div></div>';
		} else {
			popup_content += '<div class="popup_edificio_piano_box" data-id = "'+piani[i]['id']+'" data-edificio = "'+edificio_id+'"><div  class="popup_edificio_piano">'+piani[i]['nome']+'</div></div>';
		}
	}
	$('.popup_edificio_content').html(popup_content);

	$('.popup_edificio_close').html('CHIUDI');
	$('.popup_edificio_add').html('AGGIUNGI PIANO');

	$('body').off('click touch', '.popup_edificio_close');
	$('body').off('click touch', '.popup_edificio_add');


	$('body').on('click touch', '.popup_edificio_close', function () {
		$('.popup_edificio_background').css({
			'display': 'none'
		});
	});

	$('body').on('click touch', '.popup_edificio_add', function() {
		var planimetria;
		var html = '<div class="popup_input_box"><div class="popup_input_label">Nome Piano</div><input class="popup_input add_piano_nome" type="text""><div class="popup_input_bottom"></div><div class="popup_input_err add_piano_nome_err"></div></div><div class="popup_upload_img"><input class="input_popup_upload_img upload_planimetria" name="upload_planimetria" type="file" accept="image/*" capture="filesystem"><div class="button_popup_upload_img">' + '<svg id="upload" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 548.18 548.18"><title>upload</title><g id="upload_box"><path class="cls-1" d="M524.33,297.35a108.06,108.06,0,0,0-61-38.68,69.83,69.83,0,0,0,11.7-39.4q0-30.27-21.41-51.68T402,146.18a69.78,69.78,0,0,0-47.39,17.7A146.34,146.34,0,0,0,219.27,73.09q-60.52,0-103.35,42.83T73.09,219.27q0,3.71.57,12.27A127.68,127.68,0,0,0,0,347.18Q0,400,37.54,437.54t90.36,37.55H438.54q45.39,0,77.51-32.12t32.12-77.52A106.12,106.12,0,0,0,524.33,297.35Zm-161.6-7.7a8.8,8.8,0,0,1-6.42,2.71h-64v100.5a9.26,9.26,0,0,1-9.13,9.13H228.4a9.26,9.26,0,0,1-9.14-9.13V292.36h-64a8.78,8.78,0,0,1-9.13-9.13,10.73,10.73,0,0,1,2.85-6.86L249.24,176.16a9.68,9.68,0,0,1,13.13,0l100.5,100.49a8.88,8.88,0,0,1,2.56,6.57A8.78,8.78,0,0,1,362.73,289.65Z"/></g></svg>' + '</div><div class="label_upload">Caricare la planimetria</div></div><div class="popup_input_err add_planimetria_err"></div>';

		$('.popup_edificio_content').html(html);

		$('.popup_edificio_close').html('INDIETRO');
		$('.popup_edificio_add').html('AGGIUNGI');

		$('body').off('click touch', '.popup_edificio_close');
		$('body').off('click touch', '.popup_edificio_add');

		$('body').on('change', '.upload_planimetria', function() {
			planimetria= $(this)[0].files[0];


			if (planimetria != null) {
				$('.label_upload').html('Planimetria selezionata');
			} else {
				$('.label_upload').html('Caricare la planimetria');
			}
		});

		$('body').on('click touch', '.popup_edificio_close', function() {
			$('body').off('click touch', '.popup_edificio_close');
			$('body').off('click touch', '.popup_edificio_add');
			$('body').off('change', '.upload_planimetria');
			caricaPopupEdificio(index);
		});
		$('body').on('click touch', '.popup_edificio_add', async function() {

            $('.add_piano_nome_err').html('');
            $('.add_planimetria_err').html('');
			//metto off per evitare multiclick
            var nome = $('.add_piano_nome').val();
			console.log(planimetria,nome);
            /*
            if(nome != null && nome != ""){
                if(planimetria != null && planimetria != ""){
                    if(!pianoEsiste(nome, piani)){
                        console.log("APPOSTO");
                        $('body').off('click touch', '.popup_edificio_add');

                        console.log('Id Edificio', edifici[index]['id']);
                        console.log('planimetria', planimetria);

                        var planimetria_encode = new FormData();
                        planimetria_encode.append('file', planimetria);

                        var arr = new Array();
                        arr[0] = nome;
                        arr[1] = edifici[index]['id'];
                        //arr[2] = planimetria_encode;
                        $('.popup_edificio_content').html('<div class="loading_gif"><img src="images/loading_blue.gif"></div>');
                        var path_file = await upload(planimetria_encode);
                        console.log(path_file);

                        if(path_file != false && !path_file.includes("false")){
                            arr[2] = path_file;
                            var esito = await insert('aggiungiPiano', JSON.stringify(arr));
                            console.log("QUA E TRUE", esito);
                            if(esito){
                                console.log("Tutto APPOSTO");
                                $('.popup_edificio_background').css({'display' : ''});
                                indirizzaModifica();
                            }
                        }
                    }else{
                        $('.add_piano_nome_err').html('Il piano esiste già.');
                        console.log("piano esiste gia");
                    }
                }else{
                    $('.add_planimetria_err').html('Inserire planimetria.');
                    console.log("planimetria vuota");
                }
            }else{
                 $('.add_piano_nome_err').html('Inserire nome piano.');
                console.log("nome vuoto");
            }
			*/
			var checkValidita = true;

			//CONTROLLI NOME PIANO
			if(nome == null || nome == "") {
				$('.add_piano_nome_err').html('Inserire nome piano.');
				checkValidita = false;
			} else {
				if(pianoEsiste(nome, piani)) {
					$('.add_piano_nome_err').html('Il piano esiste già.');
					checkValidita = false;
				}
			}

			if(planimetria == null || planimetria == "") {
				$('.add_planimetria_err').html('Inserire planimetria.');
				checkValidita = false;
			} else {
				var planimetria_encode = new FormData();
				planimetria_encode.append('file', planimetria);

				var arr = new Array();
				arr[0] = nome;
				arr[1] = edifici[index]['id'];
				$('.label_upload').html('<span style="color: rgb(2, 123, 211)">Verifica dell\'Immagine</span>');
				var path_file = await upload(planimetria_encode);
				$('.label_upload').html('Caricare la planimetria');
				console.log(path_file);

				if(path_file.includes("#ERR:")){
					$('.add_planimetria_err').html(path_file.slice(5));
					checkValidita = false;
				}

			}

			if (checkValidita) {
				$('.popup_edificio_content').html('<div class="loading_gif"><img src="images/loading_blue.gif"></div>');
				arr[2] = path_file;
				var esito = await insert('aggiungiPiano', JSON.stringify(arr));
				console.log("QUA E TRUE", esito);
				if(esito){
					console.log("Tutto APPOSTO");
					$('.popup_edificio_background').css({'display' : ''});
					indirizzaModifica();
				} else {
					alert('Errore nella creazione del piano');
				}
			}
		});
	});

	$('body').on('click', '.popup_edificio_piano_menu', function(e) {
		if(list_page['modifica']) {
			var idPiano = $(this).parent().data()['id'];
			console.log("idPiano", idPiano);
			openContextMenu(e,idPiano, index);
		}
	});
	$('body').on('contextmenu', '.popup_edificio_piano_box', function(e) {
		if(list_page['modifica']) {
			e.preventDefault();
			var idPiano = $(this).data()['id'];
			openContextMenu(e, idPiano, index);
		}
	});

	$('body').on('click', '.info_edificio', function() {
		console.log('EDIFICIO', $(this).attr('data-edificio'));

		var index_edicici = $(this).attr('data-edificio');
		var dati_edificio = edifici[index_edicici];

		var id_edificio = dati_edificio['id'];
		caricaPopupAddEdificio(false, id_edificio);

		$('.popup_add_edificio_nome').val(dati_edificio['nome']);
		$('.popup_add_edificio_miur').val(dati_edificio['codice_miur']);
		$('.popup_add_edificio_indirizzo').val(dati_edificio['indirizzo']);
		$('.popup_add_edificio_civico').val(dati_edificio['civico']);
		$('.popup_add_edificio_cap').val(dati_edificio['cap']);
	});
}

function pianoEsiste(nomePiano,piani){

    for(var i = 0; i < piani.length; i++){
        if(piani[i].nome.toUpperCase() == nomePiano.toUpperCase()){
            return true;
        }
    }
    return false;
}

var check_time_edificio = false;
var time_edificio = null;
$('body').on('mousedown touchstart', '.edificio_icona', function(e) {
	if (onMobile()) {
		$('.content').css({'overflow' : 'hidden'});
	}
	console.log('aggiornato');
	clearTimeout(time_edificio);
	time_edificio = null;
	var selected_edificio = $(this).parent();
	var check_elimina = false;
	if (list_page['modifica']) {
		time_edificio = setTimeout(function() {
			$('.content').css({'overflow' : 'hidden'});
			var delete_edificio_icon_pos = new Array();
				delete_edificio_icon_pos['top'] = $('.delete_edificio_icon').offset().top;
				delete_edificio_icon_pos['right'] = $('.delete_edificio_icon').offset().right;
				delete_edificio_icon_pos['bottom'] = $('.delete_edificio_icon').offset().bottom;
				delete_edificio_icon_pos['left'] = $('.delete_edificio_icon').offset().left;

			check_time_edificio = true;
			$('.popups').append('<div class="new_edificio edificio" data-index="' + $(selected_edificio).attr('data-index') + '">' + $(selected_edificio).html() + '</div>');
			$('.new_edificio').css({'top' : $(selected_edificio).offset().top + 'px', 'left' : $(selected_edificio).offset().left + 'px'});
			$(selected_edificio).css({'visibility' : 'hidden'});

			$('.delete_edificio_icon').animate({'bottom' : '20px'});

			$('body').on('mousemove touchmove', '.content', function(e) {
				console.log('mi sto muovendo');
				$('.new_edificio').css({'top' : (getLayerY(e) - 125) + 'px', 'left' : (getLayerX(e) - 125) + 'px'});

				var distance_top = ( $('.new_edificio').offset().top + 125 ) - $('.delete_edificio_icon').offset().top;
				var distance_left = ( $('.new_edificio').offset().left + 125 ) -  $('.delete_edificio_icon').offset().left;
				var distance_right =  ( $('.delete_edificio_icon').offset().left + $('.delete_edificio_icon').innerWidth() ) - $('.new_edificio').offset().left;
				if ( (distance_top>=0) && (distance_left>=0) && (distance_right>=0)) {
					$('.new_edificio .edificio_icona').css({'border' : '3px solid #951515'});
					check_elimina = true;
				} else {
					$('.new_edificio .edificio_icona').css({'border' : ''});
					check_elimina = false;
				}
			});
			$('body').on('mouseup touchend', '.content', async function() {
				console.log('mouseup on content')
				clearTimeout(time_edificio);
				time_edificio = null;
				$('.content').css({'overflow' : ''});
				$('.edificio').css({'visibility' : ''});
				$('.new_edificio').remove();
				console.log('mouseup');
				$('body').off('mousemove touchmove', '.content');
				$('body').off('mouseup touchend', '.content');
				$('.delete_edificio_icon').animate({'bottom' : '-80px'});

				if (check_elimina) {
					if(await popup('Sei sicuro di voler eliminare l\'edificio selezionato?', 'SI', 'NO')) {
						//.suca.eliminaedificio
						var id = $(selected_edificio).attr('data-index');
						var esito = await insert("deleteEdificio", edifici[id].id);
						console.log(esito);
						if(esito){
							indirizzaModifica();
						}else{
							console.log("cazzo");
						}
						//code goes here...
					}
				}
			})
		}, 300);
	}
});

$('body').on('mouseup', '.edificio_icona', function(e) {
		clearTimeout(time_edificio);
		time_edificio = null;
		$('.content').css({'overflow' : ''});
	/*
		$('body').off('mousemove touchmove', '.content');// .suca.0411
		$('body').off('mouseup touchend', '.content');// .suca.0411
	*/
		if (check_time_edificio) {
			check_time_edificio = false;
		} else {
			check_time_edificio = false;

			var index = Number($(this).parent().attr('data-index'));

			caricaPopupEdificio(index);

			$('body').on('click touch', '.popup_edificio_background', function(e) {
				closeClickOutside(e, '.popup_edificio_background', '.popup_edificio', ['.popup_edificio_piano_menu', '.popup_edificio_piano_box']);
			});

			$('.popup_edificio_background').css({'display' : 'block'});

			/*$('body').on('click touch', '.popup_edificio_piano_menu', function(e) {
				if(list_page['modifica']) {
					var idPiano = $(this).parent().data()['id'];
					console.log("idPiano", idPiano);
					openContextMenu(e,idPiano, index);
				}
			});
			$('body').on('contextmenu', '.popup_edificio_piano_box', function(e) {
				if(list_page['modifica']) {
					e.preventDefault();
					var idPiano = $(this).data()['id'];
					openContextMenu(e, idPiano, index);
				}
			});*/
		}
});
function openContextMenu(e, idPiano, index) {
	console.log('click');
	$('.context_menu').remove();
	html= '<div class="context_menu" data-piano ='+idPiano+'><div class="context_menu_voice" data-id="apri">Modifica plitems</div><div class="context_menu_voice" data-id="cambia_img">Cambia immagine plitems</div><div class="context_menu_separator"></div><div class="context_menu_voice" data-id="rinomina">Rinomina piano</div><div class="context_menu_voice context_menu_voice_delete" data-id="elimina_piano">Elimina piano</div></div>';
	$('.popups').append(html);

	$('body').on('click', '.context_menu_voice', async function(){
        $('body').off('click', '.context_menu_voice');

		var action = $(this).attr('data-id');
		var idPiano = $(this).parent().data()["piano"];
		var idPlitems = await call("plitems_id", idPiano);
        var piano;
//        for(var i = 0; i<edifici[idEdificio].piani.length; i++){
//            var current_piano = edifici[idEdificio].piani[i];
//            if(current_piano.id == idPiano){
//                piano = current_piano.piani[i];
//            }
//        }

		switch(action){
			case 'apri':
				apriModifica(idPlitems, piano);
                $('body').off('contextmenu', '.popup_edificio_piano_box');
                $('body').off('click touch', '.popup_edificio_piano_menu');
				break;

//			case 'elimina_pl':
//				if (await popup('Sei sicuro di voler eliminare il plitems del piano?', 'SI', 'NO') ) {
//					$('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
//					$('.popup_edificio_background').css({'display' : 'none'});
//					await insert("eliminaPlitems", idPlitems);
//					indirizzaModifica();
//				}
//				break;

			case 'rinomina':
				$('.popup_edificio_content').html('<div class="popup_input_box"><div class="popup_input_label">Nuovo nome Piano</div><input class="popup_input popup_rename_input" type="text"><div class="popup_input_bottom"></div></div>');

				$('.popup_edificio_close').html('INDIETRO');
				$('.popup_edificio_add').html('RINOMINA');

				$('body').off('click touch', '.popup_edificio_close');
				$('body').off('click touch', '.popup_edificio_add');

				$('body').on('click touch', '.popup_edificio_close', function() {
					$('body').off('click touch', '.popup_edificio_close');
					$('body').off('click touch', '.popup_edificio_add');
					caricaPopupEdificio(index);
				});
				$('body').on('click touch', '.popup_edificio_add', async function() {
					$('body').off('click touch', '.popup_edificio_close');
					$('body').off('click touch', '.popup_edificio_add');
					var arr = new Array();
					arr[0] = idPiano;
					arr[1] = $('.popup_rename_input').val();
					$('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
					$('.popup_edificio_background').css({'display' : 'none'});
					await insert("rinominaPiano", JSON.stringify(arr));
					indirizzaModifica();
				});
				break;

			case 'elimina_piano':
				if (await popup('Sei sicuro di voler eliminare il piano', 'SI', 'NO') ) {
					var arr = new Array();
					arr[0] = idPiano;
					arr[1] = idPlitems;
					$('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
					$('.popup_edificio_background').css({'display' : 'none'});
					await insert("eliminaPiano", JSON.stringify(arr));
					indirizzaModifica();
				}
				break;
			case 'modifica_usr':
				break;

		}
	});

	$('.context_menu').css({'top' : getLayerY(e)+'px', 'left' : (getLayerX(e) - 250)+'px'});

	$('body').on('click', '.popups', function() {
		console.log('context_menu OFF')
        /*
		$('body').off('contextmenu', '.popup_edificio_piano_box');
		$('body').off('click touch', '.popup_edificio_piano_menu');
        */
		$('body').off('click touch', '.context_menu_voice');
		$('.context_menu').remove();
		$('body').off('click touch', '.popups');
	});
}

$('body').on('click', '.popup_apply',async function () {

	var elemento = elements[selected_item];
	console.log(elemento);

	var checklistvalida = true;
	if(list_page['modifica']){
		for(var i = 0; i < elemento.attributi.length; i++){
			var val = $("#a_"+i).val();
			elemento.attributi[i].valore = val;
		}
		var codice = $(".popup_add_codice").val();
		elemento.cod = codice;

		$(".el_"+selected_item).remove();
		elements[selected_item].zoom = zoom_perc;
		elements[selected_item].init(".planimetria");
	}

	if(list_page['verifica']){
		console.log("MERDA");
		for(var i = 0; i < elemento.checklist.length; i++){
			var check = elemento.checklist[i];
			var val = $("input[name='check"+i+"']:checked").val();
			if(val != null){
				check.valore = val;
			}
		}
		var note = $('#a_10').val();
		console.log(note);

		var arr = new Array;
			arr[0] = note;
			arr[1] = elemento.elemento;
		if(await insert("campo_note", JSON.stringify(arr))){
			elemento.attributi[10].valore = note;
		}

		//controllo validita qui
		checklistvalida = elemento.checkValidita();
	}

	if(list_page['modifica']){
		$('.popup_background').css({
			'display': 'none'
		});
	}else if(checklistvalida == 3){
		salvaVerificaItem(elemento);
		aggiornaBadge(elemento.arrIndex);
		$('.popup_background').css({
			'display': 'none'
		});
	}else{
		console.log('jesu');
		/*
		if(confirm('checklist non valida, vuoi uscire comunque?')){
			$('.popup_background').css({
				'display': 'none'
			});
		}
		*/
		$('body').off('click touch', '.popup_background');
		console.log('Aspetto la risposta...');
		if(await popup('Checklist non valida o incompleta, vuoi uscire comunque?', 'SI', 'NO')){
            //
            elemento.completa();
			salvaVerificaItem(elemento);
			aggiornaBadge(elemento.arrIndex);
			$('.popup_background').css({
				'display': 'none'
			});
		}
		/*
		do {
			console.log('Aspetto la risposta...');
		} while (popup_result == null);
		*/
		//console.log('La risposta è: '+popup_result);
	}

	console.log(elemento);
});

$('body').on('click touch', '.popup_close', function() {
	$('.popup_background').css({
		'display': 'none'
	});
});

$(window).resize(function() {
	if(edifici != null)	adatta_edifici();
});