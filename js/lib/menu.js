// GESTIONE MENU ----------------------------------------------------------------------------------------------------
var notification_open= true;
$('.notification_button').on('click touch', function() {
	$('.menu_content').animate({'left': '-350px'},200);
	$('.voice_close').animate({'left': '-350px'},200);
	$('.content').off();
	$('.notification_popup').show('fast', function() {
		$('body').on('click touch', '.notification_espandi', function() {
			controlloStrumenti();
		});
		$('.content').on('click touch', function(e) {

			offset_left= $('.notification_contatore').offset().left;
			offset_top= $('.notification_contatore').offset().top;
			width= $('.notification_contatore').innerWidth();
			height= $('.notification_contatore').innerHeight()+$('.notification_box').innerHeight()+$('.notification_espandi').innerHeight();


			if (((getLayerX(e)<offset_left) || (getLayerX(e)>offset_left+width)) || ((getLayerY(e)<offset_top) || (getLayerY(e)>offset_top+height))) {
				$('.notification_popup').css({'display' : 'none'});
				$('body').off('click touch', '.notification_espandi');

				$('.content').off('click');
			}
		});
	});
});
var popup_result = null;
function notifichePopup(notifiche) {
	console.log(notifiche);
	console.log(notifiche.length);
	/*
		*GEST_CIFRE: allinamento del numero di notifiche nell'SVG.
	*/
	console.log('ci entra');

	if (notifiche.length>99) {
		$('#notification #num .cls-2').html('99+');
		$('#notification #num text').attr('transform','translate(430 818)');  //con 3 cifre *GEST_CIFRE
	} else {
		if (notifiche.length<10) {
			$('#notification #num text').attr('transform','translate(610 818)'); //con 1 cifra *GEST_CIFRE
		} else {
			$('#notification #num text').attr('transform','translate(536 818)'); //con 2 cifre *GEST_CIFRE
		}
		$('#notification #num .cls-2').html(notifiche.length);
	}
	$('.notification_contatore').html(notifiche.length+' notifiche non lette');

	var code = '';
	for (var i=0; i<notifiche.length; i++) {
		var notifica_code = '<div class="notification_element" ><div class="notification_element_title"><table><tr><td class="text"><div><span style="font-weight: bold">'+notifiche[i]['codice']+' - </span>' + notifiche[i]['tipo'] + ' in ' + notifiche[i]['edificio'] + ' al ' + notifiche[i]['piano'] + '</div></td><td class="indicator"><div style="background-color: #951515"></div></td></tr></table></div></div>';
		//console.log('notifica', notifica_code);
		code += notifica_code;
	}
	//console.log('codice', code);
	$('.notification_box').html(code);

	//$('')
}

function popup(testo, positivo, negativo) {
	var code = '';
	var code_message= "<div class='popup_messaggio'>"+testo+"</div>";
	var code_actions= "<div class='popup_azioni'><div class='popup_azioni_elements popup_accept'>"+positivo+"</div><div class='popup_azioni_elements popup_reject'>"+negativo+"</div></div>";

	code= '<div class="popup">' + code_message + code_actions + '</div>';

	$('.filigrana').html(code);
	$('.filigrana').css({'display' : 'block'});

	console.log($('body').innerHeight()/2+' '+$('.popup').innerHeight()/2);
	var margin_top= $('body').innerHeight()/2-$('.popup').innerHeight()/2;
	$('.popup').css({'top' : margin_top+'px'});

	$(window).resize(function() {
		margin_top= $('body').innerHeight()/2-$('.popup').innerHeight()/2;
		$('.popup').css({'top' : margin_top+'px'});
	});
	return new Promise(function(resolve,reject) {
    	$('body').on('click touch', '.filigrana', function(e) {
			try {
				offset_left= $('.popup').offset().left;
				offset_top= $('.popup').offset().top;
				width= $('.popup').innerWidth();
				height= $('.popup').innerHeight();


				if (((e.pageX<offset_left) || (e.pageX>offset_left+width)) || ((e.pageY<offset_top) || (e.pageY>offset_top+height))) {
					$('.filigrana').css({'display' : ''});
					$(window).off('resize');
					$('body').off('click touch', '.filigrana');
				}
			} catch(e) {
			}

			resolve(false);
		});

		$('.popup').on('click touch', '.popup_accept', function() {
			$(window).off('resize');
			$('body').off('click touch', '.filigrana');
			$('.filigrana').css({'display' : ''});

			resolve(true);
		});

		$('.popup').on('click touch', '.popup_reject', function() {
			$(window).off('resize');
			$('body').off('click touch', '.filigrana');
			$('.filigrana').css({'display' : ''});

			resolve(false);
		});
    });

}
$('.menu_icon').on('click touch', function() {
	$('.notification_popup').css({'display' : 'none'});

	$('.menu_content').animate({'left': '0'},200);
	$('.voice_close').animate({'left': '0'},200);
	$('body').on('click touch', function(e) {
		if(getLayerX(e)>$('.menu_content').innerWidth()) {
			$('.menu_content').animate({'left': '-350px'},200);
			$('.voice_close').animate({'left': '-350px'},200);
		}
	});
});
function offEventStrumento() {
	$('body').off('blur change', '.zoom_percentuale_cifra');
	$('body').off('change input blur', '.slider');
	//$('body').off('click touch', '.popup_close');
	$('body').off('click touch', '.arrow_left'); //modifica
	$('body').off('click touch', '.arrow_right'); //modifica
	$('body').off('mousedown touchstart', '.item_rappr'); //modifica
	$('body').off('mouseup touchend', '.planimetria'); //modifica
	$('body').off('mouseleave', '.planimetria'); //modifica
	$('body').off('click', '.planimetria'); //modifica
	$('body').off('mousedown touchstart', '.dragscroll');
	$('body').off('mouseup touchend', '.dragscroll');
	$('body').off('mouseleave', '.dragscroll');
	$('body').off('mousedown touchstart', '.workspace'); //MOBILE
	$('body').off('mouseup touchend', '.workspace'); //MOBILE
	$('body').off('mousedown touchstart', '.item_element'); //modifica
	$('body').off('mouseup', '.item_element'); //modifica
	$('body').off('click touch', '.toolbar_elimina'); //modifica
	$('body').off('click touch', '.toolbar_resize'); //modifica
	$('body').off('click touch', '.item_rappr'); //modifica
	$('body').off('touch click', '.toolbar_layer');
	$('body').off('mouseup', '.items_table'); //modifica
}
$('.voice_menu').on('click', function() {
    if ($(this).hasClass('voice_close')) {
        return;
    }
	if (list_page['verifica'] || list_page['modifica']) {
		chiudiStrumenti();
		offEventStrumento();
	}
	for (var key in list_page) {
		list_page[key] = false;
	}

	var elem= this;
	var pagina= $(elem).attr('data-link');
	list_page[pagina] = true;


	$('.menu').css({'background-color' : $(elem).attr('data-color')});
	$('.menu_user .cls-2').css({'fill' : $(elem).attr('data-color')})
	$('.page_title div').html($(elem).attr('data-title'));
	$('.voice_close').animate({'left': '-350px'},200);
	$('.menu_content').animate({'left': '-350px'},200, function() {
		$('.load_bar').css({'background-color' : $(elem).attr('data-color')});
	});

	$('.content').attr('class', 'content');
	$('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
	if (list_page['verifica'])	indirizzaVerifica();
	if (list_page['modifica'])	indirizzaModifica();
	if (list_page['home']) 		indirizzaHome();
	if (list_page['utenti'])	indirizzaUtenti();
});

$('.voice_close').on('click', async function() {
	if( await popup("Sei sicuro di voler uscire da ASAPP.", "SI", "NO") ){
		sessionStorage.clear();
		window.location.replace("login.html");
	}
});