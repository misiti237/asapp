//LOGIN
var storage = window.sessionStorage;
var username = storage.getItem('username');
var password = storage.getItem('password');

$().ready(function() {
	if (username == null && password == null) {
		window.location.replace( SERVER_PATH + "login.html");
	}else{
        $('.content').html('<div class="loading_gif"><img src="images/loading.gif"></div>');
		indirizzaHome();
		controlloStrumenti();
		//console.log(checkScadenza());
	}
});

// INDIRIZZAMENTO --------------------------------------------------------------------------------------------------------------------------------------
function loadBar(perc) {
	$('.load_bar').animate({'width': perc+'%'},500, function() {
		if (perc>=100) {
			$('.load_bar').css({'width' : '0'});
		}
	});
}
var list_page = new Array();
list_page['home'] = false;
list_page['modifica'] = false;
list_page['verifica'] = false;
list_page['utenti'] = false;
list_page['verifica'] = false;
list_page['statistiche'] = false;
list_page['storico'] = false;
list_page['impostazioni'] = false;
async function indirizzaHome() {
	var content_html='';

	try{
		//ADATTAMENTO MENU
		var livello= await call('livelli_descrizione');
		if (livello == 'Utente Esperto') {
			$('.voice_utenti').remove();
		}
		if (livello == 'Utente Base') {
			$('.voice_utenti').remove();
			$('.voice_struttura').remove();
		}

		var nome_istituto = await call('istituti_denominazione');
		var codice_miur = await call('istituti_codice_miur');
		var preside = await call('istituti_dirigente_nome') +" "+ await call('istituti_dirigente_cognome');
		loadBar(45);

		var nome_utente = await call('utenti_nome') +" "+ await call('utenti_cognome');
		var email = await call('utenti_email');
		var ruolo = await call('livelli_descrizione');
		loadBar(90);
	}catch(error){

	}

	content_html= '<div class="home_istituto"> <div class="home_titolo">Istituto</div> <div class="home_banner_content"><div class="istituto_nome istituto_campo"><div class="home_subtitle ">Nome:</div> '+nome_istituto+'</div> <div class="istituto_miur istituto_campo"><div class="home_subtitle">Codice:</div> '+codice_miur+'</div> <div class="Istituto_preside istituto_campo"><div class="home_subtitle">Dirigente Scolastico:</div> '+preside+'</div></div> </div>     <div class="home_utente"> <div class="home_titolo">Utente</div> <div class="home_banner_content"><div class="utente_nome utente_campo"><div class="home_subtitle ">Nome:</div> '+nome_utente+'</div> <div class="utente_email utente_campo"><div class="home_subtitle">Codice:</div> '+email+'</div> <div class="utente_ruolo utente_campo"><div class="home_subtitle ">'+ruolo+'</div></div></div> </div> <div class="home_logo"></div>';

	$('.content').html(content_html);
	loadBar(100);
}

function chiudiStrumenti() {
	offEventStrumento();
	$('.content').html('');

	var elem= $('.voice_home');
	var pagina= $(elem).attr('data-link');

	$('.menu').css({'background-color' : $(elem).attr('data-color')});
	$('.menu_user .cls-2').css({'fill' : $(elem).attr('data-color')})
	$('.page_title div').html($(elem).attr('data-title'));

	$('.menu_content').animate({'left': '-350px'},200, function() {
		$('.load_bar').css({'background-color' : $(elem).attr('data-color')});
		$('.load_bar').animate({'width': '100%'},3000, function() {
			$('.load_bar').css({'width' : '0'});
		});
	});

	$('.content').attr('class', 'content');
}
function goBackStrumento() {
    offEventStrumento();
    $('.content').html('');
    if(list_page['modifica'])	indirizzaModifica();
    if(list_page['verifica'])	indirizzaVerifica();
    $('.content').attr('class', 'content');
}
$('body').on('click touch', '.go_back_button', async function() {
    if (list_page['modifica']) {
        if (await popup('Sei sicuro di voler uscire senza salvare le modifiche fatte?', 'SI', 'NO')) {
            goBackStrumento();
        }
    } else {
        goBackStrumento();
    }
});
