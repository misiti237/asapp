//CONTROLLER
//Libreria per prendere i dati dalla tabella utenti

function call(req, dati = "") {
    return new Promise(function(resolve,reject){
      $.ajax({
          url: SERVER_PATH + "/php/getAll.php",
          data: {
            "ask": req,
            "user": storage.getItem("username"),
            "psw": storage.getItem("password"),
            "data": dati
          },
          type: "POST",
          success: function(data){
            resolve(data);
						console.log('*GM-TEST', data)
          }
      });
    });
}

function insert(req, dati){
  console.log(dati);
	return new Promise(function(resolve,reject){
		$.ajax({
				url: SERVER_PATH + "/php/setAll.php",
				data:{
					"set": req,
					"user": storage.getItem("username"),
					"psw": storage.getItem("password"),
					"data": dati
				},
				type: "POST",
				success: function(data){
					resolve(data);
				}
		});
	});
}

function upload(dati){
    console.log(dati);
	return new Promise(function(resolve,reject){
		$.ajax({
				url: SERVER_PATH + "/php/upload.php",
				processData: false,
				contentType: false,
                data: dati,
				type: "POST",
				success: function(data){
					resolve(data);
				},
                error: function(err){
                    reject('#ERR:Errore anomalo nel caricamento dell\'immagine. '+err);
                }
		});
	});
}

async function controlloStrumenti(dati = ""){
    var notifiche = new Array();
    var cont = 0;

	await $.ajax({
			url: SERVER_PATH + "/php/controllo_checklist.php",
			data:{
				"user": storage.getItem("username"),
				"psw": storage.getItem("password"),
				"data": dati
			},
			type: "POST",
			success: function(data){
				console.log('modificato 2');

				var notifiche_tmp = new Array();
				notifiche_tmp = JSON.parse(data);
				for (var i=0; i<notifiche_tmp.length; i++) {
				    notifiche.push(notifiche_tmp[i]);
				    cont++;
				}
				//notifichePopup(JSON.parse(data));
				//console.log(data);
			},
			error: function(err){
			}
	});

	await $.ajax({
			url: SERVER_PATH + "/php/controllo_scadenza.php",
			data:{
				"user": storage.getItem("username"),
				"psw": storage.getItem("password")
			},
			type: "POST",
			success: function(data){
				//elementi scaduti
				console.log('LE SCADENZE');

				var notifiche_tmp = new Array();
				notifiche_tmp = JSON.parse(data);
				for (var i=0; i<notifiche_tmp.length; i++) {
				    notifiche.push(notifiche_tmp[i]);
				    cont++;
				}

			},
			error: function(err){
			}
	});

	notifichePopup(notifiche);
}


function checkScadenza(){
	$.ajax({
			url: SERVER_PATH + "/php/controllo_scadenza.php",
			data:{
				"user": storage.getItem("username"),
				"psw": storage.getItem("password")
			},
			type: "POST",
			success: function(data){
				//elementi scaduti
				console.log('LE SCADENZE');
				return JSON.parse(data);

			},
			error: function(err){
			}
	});
}
