var utenti;

function openContextMenuUser(e, idUser, index) {
	console.log('click');
	$('.context_menu').remove();
	html= '<div class="context_menu" data-utente =' + idUser + '><div class="context_menu_voice" data-id="modifica_usr">Modifica dati</div><div class="context_menu_separator"></div><div class="context_menu_voice context_menu_voice_delete" data-id="elimina_usr">Elimina utente</div></div>';
	$('.content').append(html);

	$('.context_menu').css({'top' : getLayerY(e)+'px', 'left' : (getLayerX(e) - 250)+'px'});
	$('body').on('click touch', '.content', function() {
		$('.context_menu').remove();
		$('body').off('click touch', '.content');
	});
	$('body').on('click touch', '.context_menu_voice', async function(){
		var action = $(this).attr('data-id');
		console.log(action, idUser);
		switch (action) {
			case 'modifica_usr':
				caricaPopupGestisciUtenti(index,'mod');
				break;
			case 'elimina_usr':
				if (await popup('Sei sicuro di voler eliminare l\'utente', 'SI', 'NO') ) {
					if(await insert("eliminaUtente", idUser)) indirizzaUtenti();
				}
				break;
		}
	});
}

async function addUser(req, nome, cogn, email, psw, cod_fisc, livello){
	var arr = new Array();
	arr[0] = req;
	arr[1] = nome;
	arr[2] = cogn;
	arr[3] = email;
	arr[4] = psw;
	console.log(arr[4])
	arr[5] = cod_fisc;
	arr[6] = livello;
	console.log(arr);
	//console.log(JSON.stringify(arr));
	var esito = await insert('azioniUtente', JSON.stringify(arr));
	if(esito){
		console.log("SUCCESSO");
		indirizzaUtenti();
	}else{
		console.log(esito);
	}

}

function mascheraUtenti(utenti) {
	var code = '';
	for(var i=0; i<utenti.length; i++) {
		var utente = utenti[i];
        var context_menu_code =  '<svg id="context_menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>#context_menu .cls-1{fill:#464646;}</style></defs><title>context_menu</title><g id="context_menu_body"><rect class="cls-1" x="216.75" y="407" width="78.5" height="78.5"/><rect class="cls-1" x="217.75" y="217" width="78.5" height="78.5"/><rect class="cls-1" x="216.75" y="27" width="78.5" height="78.5"/></g></svg';
		if (i%2 == 0) {
		   code += '<tr data-index = '+i+' data-id=' + utente['id'] + '><td>' + utente['nome'] + '</td><td>' + utente['cognome'] + '</td><td>' + /*utente['accesso']*/utente['ultimo_accesso'] + '</td><td class="utenti_table_livello">' + utente['livello'] + '</td><td class="utenti_table_action">' + context_menu_code + '</td></tr>'
		} else {
			code += '<tr data-index = '+i+' data-id=' + utente['id'] + ' class="table_cell_dark"><td>' + utente['nome'] + '</td><td>' + utente['cognome'] + '</td><td>' + /*utente['accesso']*/utente['ultimo_accesso'] + '</td><td class="utenti_table_livello">' + utente['livello'] + '</td><td class="utenti_table_action">' + context_menu_code  + '</td></tr>'
		}

	}
    $('body').on('click touch', '.utenti_table_action', function(e) {
       openContextMenuUser(e, $(this).parent().attr('data-id'), $(this).parent().attr('data-index'));
    });
	return code;
}
function caricaPopupGestisciUtenti(index, mod) {
	$('.popup_utenti_nome').html('Modifica dati utenti');
	if (mod == 'mod') {
		popup_content = '<div class="popup_input_box"><div class="popup_input_label">Nome</div><input class="popup_input popup_add_utenti_nome" type="text" value="'+utenti[index].nome+'"><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Cognome</div><input class="popup_input popup_add_utenti_cognome"  type="text" value="'+utenti[index].cognome+'"><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><input class="popup_add_utenti_type" name="popup_add_utenti_type" type="radio" value="Utente Base" id="type_standard" checked><label for="type_standard">Utente Standard</label> <br> <input class="popup_add_utenti_type" name="popup_add_utenti_type" type="radio" value="Utente Esperto" id="type_esperto"><label for="type_esperto">Utente Esperto</label></div>'+'<div class="popup_input_box"><div class="popup_input_label">Email</div><input class="popup_input popup_add_utenti_email" type="text" value="'+utenti[index].email+'" readonly><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Password</div><input class="popup_input popup_add_utenti_password" type="password" value="'+utenti[index].password+'"><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Codice Fiscale</div><input class="popup_input popup_add_utenti_cf" type="text" value="'+utenti[index].codice_fiscale+'"><div class="popup_input_bottom"></div></div>';
	}
	if (mod == 'add') {
		popup_content = '<div class="popup_input_box"><div class="popup_input_label">Nome</div><input class="popup_input popup_add_utenti_nome" type="text" value=""><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Cognome</div><input class="popup_input popup_add_utenti_cognome"  type="text" value=""><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><input class="popup_add_utenti_type" name="popup_add_utenti_type" type="radio" value="Utente Base" id="type_standard" checked><label for="type_standard">Utente Standard</label> <br> <input class="popup_add_utenti_type" name="popup_add_utenti_type" type="radio" value="Utente Esperto" id="type_esperto"><label for="type_esperto">Utente Esperto</label></div>' + '<div class="popup_input_box"><div class="popup_input_label">Email</div><input class="popup_input popup_add_utenti_email" type="text" value=""><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Password</div><input class="popup_input popup_add_utenti_password" type="password" value=""><div class="popup_input_bottom"></div></div>' + '<div class="popup_input_box"><div class="popup_input_label">Codice Fiscale</div><input class="popup_input popup_add_utenti_cf" type="text" value=""><div class="popup_input_bottom"></div></div>';
	}
	$('.popup_utenti_content').html(popup_content);

    $('.popup_utenti_close').html('INDIETRO');
    if (mod == 'mod') {
		$('.popup_utenti_add').html('MODIFICA');
	}
	if (mod == 'add') {
		$('.popup_utenti_add').html('AGGIUNGI');
	}

    $('body').on('click touch', '.popup_utenti_background', function(e) {
		closeClickOutside(e, '.popup_utenti_background', '.popup_utenti');
	});

	$('.popup_utenti_background').css({'display' : 'block'});

    $('body').off('click touch', '.popup_utenti_close');
    $('body').off('click touch', '.popup_utenti_add');

    $('body').on('click touch', '.popup_utenti_close', function() {
        $('body').off('click touch', '.popup_utenti_close');
        $('body').off('click touch', '.popup_utenti_add');

        $('.popup_utenti_background').css({'display' : 'none'});
    });
    $('body').on('click touch', '.popup_utenti_add', async function() {
		var nome = $(".popup_add_utenti_nome").val();
		var cogn = $(".popup_add_utenti_cognome").val();
		var email = $(".popup_add_utenti_email").val();
		var psw = $(".popup_add_utenti_password").val();
		var fisc = $(".popup_add_utenti_cf").val();
		var perm = $(".popup_add_utenti_type:checked").val();
		console.log(perm);
		//var perm = 'Utente Base';
		if (mod == 'mod') {
			console.log("MODIFICo");
			addUser('mod', nome, cogn, email, psw, fisc, perm);
		}
		if (mod == 'add') {
			console.log("AGGIUNGO");
			addUser('add', nome, cogn, email, psw, fisc, perm);
		}
    });
}
async function indirizzaUtenti() {
	var content_html='';
	content_html= '<div class="utenti_box"><div class="utenti_search"><input name="utenti" class="utenti_input" placeholder="Cerca utente"><div class="utenti_search_button"></div></div><div class="utenti_list"><table class="utenti_list_table"><tr class="utenti_list_caption"><td>Nome</td><td>Cognome</td><td>Ultimo accesso</td><td>Livello</td><td class="utenti_table_caption_action"></td></tr></table></div></div><div class="add_utente">AGGIUNGI UTENTI</div>';
	$('.content').html(content_html);
	utenti = await call('all_users');
	if(utenti != "false"){
		utenti = JSON.parse(utenti);
		for(var i=0; i  <utenti.length; i++){
			if(utenti[i]['livello'] == 2) utenti[i]['livello'] = "Utente Esperto";
			if(utenti[i]['livello'] == 3) utenti[i]['livello'] = "Utente Base";
		}
		$('.utenti_list_table').append(mascheraUtenti(utenti));
	}else{
		$('.utenti_list_table').append("NON HAI I PERMESSI");
	}
	var popups='<div class="popup_utenti_background"><div class="popup_utenti"><div class="popup_utenti_nome"></div><div class="popup_utenti_content"></div><div class="popup_utenti_option"><div class="popup_utenti_close"></div><div class="popup_utenti_add"></div></div></div>';
	$('.popups').html(popups);

	$("body").on("keypress", ".utenti_input", function(){

		var val = $(this).val().toUpperCase();
		var newArray = new Array();
		for(var i = 0; i < utenti.length; i++){
			if(utenti[i].nome.toUpperCase().includes(val) || utenti[i].cognome.toUpperCase().includes(val)){
				newArray.push(utenti[i]);
			}
		}
		$('.utenti_list_table tr').not('.utenti_list_caption').remove();
		$('.utenti_list_table').append(mascheraUtenti(newArray));
	});

};
$('body').on('click touch', '.add_utente', function() {
	caricaPopupGestisciUtenti(null,'add');
});
$('body').on('focus', '.popup_input', function() {
	var bottom = $(this).parent().find('.popup_input_bottom');
	var label = $(this).parent().find('.popup_input_label');

	$(this).css({'color' : 'rgba(0,0,0,0.8)'});
	$(label).css({'color' : '#027BD3'});
	$(bottom).animate({'width' : '100%'});
});
$('body').on('blur', '.popup_input', function() {
	var bottom = $(this).parent().find('.popup_input_bottom');
	var label = $(this).parent().find('.popup_input_label');

	$(this).css({'color' : ''});
	$(label).css({'color' : ''});
	$(bottom).animate({'width' : '0'});
});
