function Element(i, x, y, src_img, plitemsIndex, elementoIndex, attributi, checklist, categoriaElementoIndex, cod, id, zoom, categoria) {
    this.arrIndex = i;
    this.img = src_img;
    this.id = id;
    //x e y saranno sempre in percentuale
    this.x = x;
    this.y = y;
	this.zoom = zoom;

    //valori di appoggio per la distanza del mouse dall'immagine
    //li imposto nel mousedown e li uso nella funzione move chiamata nel mousemove
    this.offL = 0;
    this.offT = 0;

    this.cod = cod;
    this.elemento = elementoIndex;
    this.categoriaElemento = categoriaElementoIndex;    //indica il tipo di strumento (estintore, naspo, ups...)
    this.categoria = categoria;                         //(antincednio, strutturale...)
    this.plitems = plitemsIndex;
    this.attributi = attributi;
    this.checklist = checklist;

    this.init = function (container) {
        //costruttore
        $(container).append('<div class="item_rappr el_' + this.arrIndex + ' elem_cat_' + this.categoria + '" data-id =' + this.arrIndex + ' style="top: ' + this.y + '%; left: ' + this.x + '%; background-image: url('+this.img+'); width: ' + this.zoom + '%; padding-top: ' + this.zoom + '%"><div class="element_badge"></div><div class="id_label">'+this.cod+'</div></div>');
        //this.adaptToContainer(container);
    }

    this.handle = function () {
        this.show();
    }

    this.show = function () {
        //i valori sono sempre in percentuale
        $(".el_" + this.arrIndex).css({
            top: this.y + "%",
            left: this.x + "%"
        });
    }

    this.adaptToContainer = function (container) {
        //richiamato quando viene istanziato l'oggetto e nella funzione move
        this.x = this.fromPixelXToPercentage(container);
        this.y = this.fromPixelYToPercentage(container);
    }

    this.fromPixelXToPercentage = function (container) {
        //container jquery format
        return (this.x * 100) / $(container).width();
    }

    this.fromPixelYToPercentage = function (container) {
        //container jquery format
        return (this.y * 100) / $(container).height();
    }

    this.move = function (xPointer, yPointer, container) {
        //xPointer e yPointer sono o per il mobile o per il desktop
        //distanza del mouse dal container
		var calcY = yPointer - ($('.el_' + this.arrIndex).innerHeight()/2);
		var calcX = xPointer - ($('.el_' + this.arrIndex).innerWidth()/2);

		if (calcY <= 0) {
			this.y = 0
		} else if(calcY >= $(container).innerHeight() - $('.el_' + this.arrIndex).innerHeight()) {
			this.y = $(container).innerHeight() - $('.el_' + this.arrIndex).innerHeight()
		} else {
			this.y = calcY;
		}

		if(calcX <= 0){
			this.x = 0;
		} else	if(calcX >= $(container).innerWidth() - $('.el_' + this.arrIndex).innerWidth()){
			this.x = $(container).innerWidth() - $('.el_' + this.arrIndex).innerWidth();
		} else {
			this.x = calcX;
		}




    //solo durante il movimento dell'oggetto, esso viene adattato al contenitore
    this.adaptToContainer($(container));
		this.show();

    }

    this.setDistMouseFromImg = function (top, left) {
        //prendo l'offset del mouse dall'immagine poichè mi servirà per lo spostamento, viene preso sul mousedown dell'elemento
        this.offT = top;
        this.offL = left;
    }
    
    //FUNZIONE "COMPLETA"
    this.completa = function(){
        for(var i = 0; i < this.checklist.length; i++){
            var check = this.checklist[i];
            if (check.valore == null || check.valore==""){
                check.valore = 2;
            }
        }
    }

    this.checkValidita = function(){
      //0 = check obbligaotrie con valori mancanti
      //1 = check obbligaotrie con valore ko
      //2 = check non obbligatorie vuote o ko
      //3 = ok
      for(var i = 0; i < this.checklist.length; i++){
  		var check = this.checklist[i];
  		if(check.valore == null || check.valore == ""){
          	if(check.obbligatorieta == 1) return 0;
          	if(check.obbligatorieta == 0) return 2;
        }else{
          if(check.obbligatorieta == 1){
            if(check.valore == 1) return 1;
          } else{
            if(check.valore == 2) return 2;
          }
        }
  	  }
      return 3;
    }


    //viene chiamata quando l'oggetto è istanziato
    this.init($(".planimetria"));
}
