<?php

    /*
        Controllo gli elementi e verifico la loro scadenza

    */


    include("./dbconnection.php");

    $usr = $_POST["user"];
    $psw = $_POST["psw"];

    $res = $objPDO->prepare("SELECT fk_istituto FROM Utenti WHERE email = '$usr' AND password = '$psw'");
    $res->execute();
    $idIsti = $res->fetchColumn();

    $output = array();

    function checkScadenza(){
        global $idIsti, $objPDO, $output;

        $res = $objPDO->prepare("SELECT e.codice, e.data_creazione, ae.valore_attributo, ed.nome as edificio_nome, p.nome as piano_nome, ce.nome_classe as tipo_elemento FROM Elementi e, Classi_Elemento ce, Attributi a, Attributi_Elementi ae, Istituti i, Edifici ed, Piani p, Items it, Plitems pl WHERE it.fk_elemento = e.id AND e.fk_classe_elemento = ce.id AND it.fk_plitems = pl.id AND e.id = ae.fk_elemento AND a.id = ae.fk_attributo AND i.id = ed.fk_istituto AND ed.id = p.fk_edifici AND pl.fk_piani = p.id AND a.id = 9 AND i.id = :istit AND ae.stato = 1 GROUP BY e.id");
        $res->bindParam(":istit", $idIsti);
        $res->execute();

        $date_now = date("d-m-Y");

        $rows= $res->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $row) {
            $scadenza = date("d-m-Y", strtotime($row['valore_attributo']));

            if($scadenza < $date_now){
                $buffer = array();

                //elemento scaduto
                $buffer['edificio'] = $row['edificio_nome'];
                $buffer['piano'] = $row['piano_nome'];
                $buffer['codice'] = $row['codice'];
                $buffer['data_creazione'] = $row['data_creazione'];
                $buffer['tipo'] = $row['tipo_elemento'];
                $buffer['scadenza'] = $row['valore_attributo'];

                array_push($output, $buffer);
            }
        }
    }

    checkScadenza();
    echo json_encode($output);

?>
