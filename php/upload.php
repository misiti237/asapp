<?php
	include("./config.php");

	header("Access-Control-Allow-Origin: *");

	$target_dir = "../plitems/"; //.suca. è un po' brutto
	$time = time();	//usato per evitare il problema delle planimetrie già esistenti --> timestamp dal (01/01/1970 00:00:00 GMT)
	$new_file_name = $time."_".basename($_FILES["file"]["name"]);
	$target_file = $target_dir.$new_file_name;

	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	// Check if file already exists
	if (file_exists($target_file)) {
		echo "#ERR:Errore anomalo/file esiste";
		exit();
	}

	// Check file size
	if ($_FILES["file"]["size"] > 8000000) {
		echo "#ERR:Dimensione file deve essere inferiore a 8 mb";
		exit();
	}

	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
		echo "#ERR:Formato file non constetito, usare jpg, jpeg, png";
		exit();
	}


	// if everything is ok, try to upload file
	if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		echo  $PATH_DIR.'plitems/'.$new_file_name;
	} else {
		echo "#ERR:Errore anomalo nel caricamento del file";
	}
?>
