<?php

    /*
        Ogni giorno alle 1.00 am controllo la scadenza delle verifiche di tutti (tutti gli istituti) gli item attivi (stato = 1).
		Se data_creazione (Verifica) + periodicità (Attributi) <= date_now() ---->
			---->stato (Verifica) = 0;
    */

    /*UPDATE Verifiche SET stato = 0 WHERE id IN (SELECT v.id FROM (SELECT id, fk_items, data_creazione FROM Verifiche WHERE stato = 1) v JOIN (SELECT it.id as id, ae.`valore_attributo` as period FROM Attributi_Elementi ae, Items it WHERE ae.fk_elemento=it.fk_elemento AND   ae.fk_attributo=10) a ON v.fk_items = a.id WHERE DATE_ADD(v.data_creazione, INTERVAL a.period MONTH) <= CURDATE())*/

    include("./dbconnection.php");


	$res = $objPDO->prepare("SELECT v.id FROM Verifiche v JOIN (SELECT it.id as id, ae.`valore_attributo` as period FROM Attributi_Elementi ae, Items it WHERE ae.fk_elemento=it.fk_elemento AND   ae.fk_attributo=10) a ON v.fk_items = a.id WHERE v.stato = 1 AND DATE_ADD(v.data_creazione, INTERVAL a.period MONTH) <= CURDATE()");
    $res->execute();
    $output = $res->fetchAll(PDO::FETCH_ASSOC);

    print_r($output);

    $res = $objPDO->prepare("UPDATE Verifiche SET stato = 0 WHERE id IN (SELECT v.id FROM (SELECT id, fk_items, data_creazione FROM Verifiche WHERE stato = 1) v JOIN (SELECT it.id as id, ae.`valore_attributo` as period FROM Attributi_Elementi ae, Items it WHERE ae.fk_elemento=it.fk_elemento AND   ae.fk_attributo=10) a ON v.fk_items = a.id WHERE DATE_ADD(v.data_creazione, INTERVAL a.period MONTH) <= CURDATE())");
    $res->execute();

    $objPDO = null;

    include("./dbconnection_log.php");

    foreach ($output as $out) {
        // echo($out['id']);
        $res = $objPDO->prepare("INSERT INTO `log_verifiche`(`id`) VALUES (:id)");
        $res->bindParam(':id', $out['id']);
        $res->execute();
    }

    $objPDO = null;

?>
