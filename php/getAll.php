<?php
  include("./dbconnection.php");
  include("./common.php");
  
  $req = $_POST["ask"];  //il tipo di elemento richiesto (utenti, items, enti, ecc...)
  $usr = $_POST["user"];
  $psw = $_POST["psw"];
  $data = $_POST["data"];

  header("Access-Control-Allow-Origin: *");

  switch ($req) {

    //TABELLA UTENTI

    //PRENDO DATI UTENTE LOGGATO

    case 'utenti_nome':
        oneFieldQuery("SELECT nome FROM Utenti WHERE email = '$usr'");
        break;
    case 'utenti_cognome':
        oneFieldQuery("SELECT cognome FROM Utenti WHERE email = '$usr'");
        break;
    case 'utenti_email':
        echo $usr;
        break;
    case 'utenti_codice_fiscale':
        oneFieldQuery("SELECT codice_fiscale FROM Utenti u WHERE u.email = '$usr'");
        break;
    case 'all_users':
        getAllUsers();
        break;
    case 'utenti_livelli':

    case 'livelli_descrizione':
        oneFieldQuery("SELECT l.descrizione FROM Livelli l, Utenti u WHERE u.email = '$usr' AND u.fk_livelli = l.id");
        break;
    case 'utenti_istituto':

    case 'istituti_denominazione':
        oneFieldQuery("SELECT i.denominazione FROM Istituti i, Utenti u WHERE u.fk_istituto = i.id AND u.email = '$usr'");
        break;

    //TABELLA ISTITUTO

    //CAMPO DENOMINAZIONE IN UTENTI_ISTITUTO

    case 'istituti_codice_miur':
        oneFieldQuery("SELECT i.codice_miur FROM Istituti i, Utenti u WHERE u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_sede_legale':
        oneFieldQuery("SELECT i.sede_legale FROM Istituti i, Utenti u WHERE u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_regione':
        oneFieldQuery("SELECT r.nome FROM Regioni r, Utenti u, Istituti i WHERE i.fk_regione = r.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;
    case 'istituti_provincia':
        oneFieldQuery("SELECT p.nome FROM Province p, Utenti u, Istituti i WHERE i.fk_provincia = p.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;
    case 'istituti_comune':
        oneFieldQuery("SELECT c.nome FROM Comune c, Utenti u, Istituti i WHERE i.fk_comune = c.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;
    case 'istituti_civico':
        oneFieldQuery("SELECT i.civico FROM Istituti i, Utenti u WHERE u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_cap':
        oneFieldQuery("SELECT i.cap FROM Istituti i, Utenti u WHERE u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_dirigente_nome':
        oneFieldQuery("SELECT r.nome FROM Istituti i, Referenze r, Utenti u WHERE i.fk_dirigente = r.id AND u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_dirigente_cognome':
        oneFieldQuery("SELECT r.cognome FROM Istituti i, Referenze r, Utenti u WHERE i.fk_dirigente = r.id AND u.fk_istituto = i.id AND u.email = '$usr'");
        break;
    case 'istituti_dsga':
        oneFieldQuery("SELECT r.nome FROM Istituti i, Referenze r WHERE i.fk_dsga = r.id ");
        break;
    case 'istituti_rspp':
        oneFieldQuery("SELECT r.nome FROM Istituti i, Referenze r WHERE i.fk_rspp = r.id ");
        break;

    //TABELLA LIVELLI

    //CAMPO DESCRIZIONE IN UTENTI_LIVELLI

    case 'livelli_permessi':
        oneFieldQuery("SELECT l.permessi FROM Utenti u, Livelli l WHERE u.fk_livelli = l.id AND u.email = '$usr' ");
        break;

    //TABELLA EDIFICI

    case 'edifici_nome':
        oneFieldQuery("SELECT e.nome FROM Edifici e, Istituti i, Utenti u WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_codice_miur':
        oneFieldQuery("SELECT e.codice_miur FROM Edifici e, Istituti i, Utenti u WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_indirizzo':
        oneFieldQuery("SELECT e.indirizzo FROM Edifici e, Istituti i, Utenti u WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_civico':
        oneFieldQuery("SELECT e.civico FROM Edifici e, Istituti i, Utenti u WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_cap':
        oneFieldQuery("SELECT e.cap FROM Edifici e, Istituti i, Utenti u WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_regione':
        oneFieldQuery("SELECT r.nome FROM Edifici e, Regioni r, Utenti u, Istituti i WHERE e.fk_regione = r.id AND e.fk_istituto = i.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_provincia':
        oneFieldQuery("SELECT p.nome FROM Edifici e, Province p, Utenti u, Istituti i WHERE e.fk_provincia = p.id AND e.fk_istituto = i.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;

    case 'edifici_comune':
        oneFieldQuery("SELECT c.nome FROM Edifici e, Comune c, Utenti u, Istituti i WHERE e.fk_comune = c.id AND e.fk_istituto = i.id AND u.fk_istituto = i.id AND u.email = '$usr' ");
        break;

	case 'edifici':
        getEdifici();
        break;



    //TABELLA PIANI

    case 'piani_nome':
        oneFieldQuery("SELECT p.nome FROM Edifici e, Istituti i, Utenti u, Piani p WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND e.id = p.fk_edifici AND u.email = '$usr' ");
        break;

	case 'piani_edificio':
        getPianiEdificio($data);
        break;

    //TABELLA PLITEMS

    case 'plitems_id':
        oneFieldQuery("SELECT pl.id FROM Piani p, Plitems pl WHERE pl.fk_piani = p.id AND p.id = '$data' AND pl.stato = 1 ");
        break;

    case 'plitems_data_ultima_modifica':
        oneFieldQuery("SELECT data_ultima_modifica FROM Plitems WHERE pl.id = '$data' ");
        break;

    case 'plitems_img_planimetria':
        oneFieldQuery("SELECT img_planimetria FROM Plitems WHERE id = '$data' ");
        break;

    case 'zoom_perc':
        oneFieldQuery("SELECT zoom_perc FROM Plitems WHERE id = '$data' ");
        break;

    case 'plitems_stato':
        oneFieldQuery("SELECT pl.stato FROM Edifici e, Istituti i, Utenti u, Piani p, Plitems pl WHERE u.fk_istituto = i.id AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND u.email = '$usr' ");
        break;

  //TABELLA CLASSI_ELEMENTO

    case 'classi_elemento':
        getClassiElemento();
        break;

    case 'categorie_elemento':
        getCategorie();
        break;

  //TABELLA ITEMS
    case 'items':
        getItems();
        break;

    //ATTRIBUTI
    case 'items_attributi':
        sendMascheraAttributi($data);
        break;

    case 'items_checklist':
        echo sendMascheraChecklist($data);
        break;

    default:
        # code...
        break;

  }



  //funzione che ritorna un solo campo

  function oneFieldQuery($sql){

    global $objPDO;

    $res = $objPDO->prepare($sql);
    $res->execute();

    //fetch column restituisce una singola colonna della query

    $out = $res->fetchColumn();

    echo "$out";

  }


  function getEdifici(){
	  global $objPDO, $usr;
	  //SELECT e.nome, e.codice_miur, e.indirizzo FROM Edifici e, Utenti u, Istituti i WHERE e.fk_istituto = i.id AND u.fk_istituto = i.id AND u.email = 'freebbitwebdesign@gmail.com'

	  $res = $objPDO->prepare("SELECT e.id, e.nome, e.codice_miur, e.indirizzo, e.civico, e.cap FROM Edifici e, Utenti u, Istituti i WHERE e.stato = 1 AND e.fk_istituto = i.id AND u.fk_istituto = i.id AND u.email = '$usr' ORDER BY e.nome ASC");
	  $res->execute();

	  //$output = array();
	  $rows= $res->fetchAll(PDO::FETCH_ASSOC);

	  echo json_encode($rows);
  }


  function getPianiEdificio($id){
	  global $objPDO;

	  $res = $objPDO->prepare("SELECT id, nome FROM Piani WHERE fk_edifici = '$id' AND stato = 1");
	  $res->execute();

	  $output = array();
	  $rows= $res->fetchAll(PDO::FETCH_ASSOC);

	  echo json_encode($rows);

  }


  function getItems(){

    global $objPDO, $data;
    $res = $objPDO->prepare("SELECT it.id, it.posX, it.posY, it.fk_plitems, it.fk_elemento, ce.src_img, e.codice, ce.fk_categoria FROM Items it, Plitems pl, Elementi e, Classi_Elemento ce WHERE it.fk_elemento = e.id AND e.fk_classe_elemento = ce.id AND it.fk_plitems = pl.id AND pl.id = '$data' AND pl.stato = 1 AND it.stato = 1 ");
    $res->execute();

    $output = array();
    $rows= $res->fetchAll(PDO::FETCH_ASSOC);

    foreach ($rows as $row) {
        $buffer = array();
        //dati items e src_img presa da tabella Classi_Elemento
        $buffer['id'] = $row['id'];
        $buffer['posX'] = $row['posX'];
        $buffer['posY'] = $row['posY'];
        $buffer['fk_plitems'] = $row['fk_plitems'];
        $buffer['fk_elemento'] = $row['fk_elemento'];
        $buffer['src_img'] = $row['src_img'];
        $buffer['codice'] = $row['codice'];
        $buffer['categoria'] = $row['fk_categoria'];

        $elemento = $buffer['fk_elemento'];
        $attributi = getAttributi($elemento);
        //Prendo checklist item (no valori)
        $checklist = getChecklist($elemento);

        $buffer['attributi'] = $attributi;
        $buffer['checklist'] = $checklist;

        array_push($output, $buffer);

    }

    echo json_encode($output);

  }



  function sendMascheraAttributi($idClasse){
    //inviare attributi data la classe elemento

    global $objPDO;
    $attributi = array();

    //Prendo attributi item e inserisco in un unico vettore che trasformerò in JSON

    $attrQry = $objPDO->prepare("SELECT a.nome, a.descrizione FROM Attributi a, Classi_Elemento_Attributi cea, Classi_Elemento ce WHERE ce.id = '$idClasse'  AND cea.fk_classi_elemento = ce.id AND cea.fk_attributi = a.id");
    $attrQry->execute();
    $attrs = $attrQry->fetchAll(PDO::FETCH_ASSOC);

    foreach ($attrs as $attr) {

      $infos = array();
      $infos['nome'] = $attr['nome'];
      $infos['valore'] = '';
      $infos['descrizione'] = $attr['descrizione'];
      array_push($attributi, $infos);

    }

    header("content-type:application/json");
    //print_r($attrs);
    echo json_encode($attributi);

  }



  function sendMascheraChecklist($idClasse){
    //inviare checklist data classe elemento
    // SELECT ck.nome_check, ck.descrizione, ck.obbligatorieta FROM Checklist ck, Classi_Elemento_Checklist cec, Classi_Elemento ce WHERE classe = ce.id AND cec.fk_classe_elemento = ce.id AND cec.fk_checklist = ck.id
    global $objPDO;
    $checklist = array();

    $checkQry = $objPDO->prepare("SELECT ck.id, ck.nome_check, ck.descrizione, ck.obbligatorieta FROM Checklist ck, Classi_Elemento_Checklist cec, Classi_Elemento ce WHERE ce.id = '$idClasse' AND cec.fk_classe_elemento = ce.id AND cec.fk_checklist = ck.id");
    $checkQry->execute();
    $checks = $checkQry->fetchAll(PDO::FETCH_ASSOC);

    foreach ($checks as $check) {

      $elementicheck = array();

      $elementicheck['id'] = $check['id'];
      $elementicheck['nome'] = $check['nome_check'];
      $elementicheck['descrizione'] = $check['descrizione'];
      $elementicheck['obbligatorieta'] = $check['obbligatorieta'];
      $elementicheck['valore'] = '';

      array_push($checklist, $elementicheck);

    }



    return json_encode($checklist);

  }



  function getChecklist($idElemento){

    global $objPDO;

    $checklist = array();

    $checkQry = $objPDO->prepare("SELECT ck.id, ck.nome_check, v.valore_check, ck.descrizione, ck.obbligatorieta FROM Checklist ck, Classi_Elemento_Checklist cec, Classi_Elemento ce, Elementi e, Verifiche v, Items it WHERE e.id = '$idElemento' AND e.fk_classe_elemento = ce.id AND cec.fk_classe_elemento = ce.id AND cec.fk_checklist = ck.id AND it.stato = '1' AND v.stato = '1' AND e.id = it.fk_elemento AND v.fk_items = it.id AND v.fk_check = ck.id");
    $checkQry->execute();

    if($checkQry->rowCount() == 0){
      //se il set è vuoto vuol dire che non ho verifiche per quell'elemento
      $idClasseQry = $objPDO->prepare("SELECT ce.id FROM Classi_Elemento ce, Elementi e WHERE ce.id = e.fk_classe_elemento AND e.id = '$idElemento'");
      $idClasseQry->execute();

      $idClasse = $idClasseQry->fetchColumn();
      $chk = json_decode(sendMascheraChecklist($idClasse), true);

      return $chk;
    }


    $checks = $checkQry->fetchAll(PDO::FETCH_ASSOC);

    foreach ($checks as $check) {

      $elementicheck = array();
      $elementicheck['id'] = $check['id'];
      $elementicheck['nome'] = $check['nome_check'];
      $elementicheck['descrizione'] = $check['descrizione'];
      $elementicheck['obbligatorieta'] = $check['obbligatorieta'];
      $elementicheck['valore'] = $check['valore_check'];

      array_push($checklist, $elementicheck);

    }

    return $checklist;

  }



  function getAttributi($idElemento){

    global $objPDO;
    $attributi = array();

    //Prendo attributi item e inserisco in un unico vettore che trasformerò in JSON

    $attrQry = $objPDO->prepare("SELECT a.nome, ae.valore_attributo, a.descrizione FROM Attributi a, Attributi_Elementi ae, Elementi e WHERE e.id = '$idElemento' AND ae.fk_elemento = e.id AND ae.fk_attributo = a.id AND ae.stato = 1");
    $attrQry->execute();
    $attrs = $attrQry->fetchAll(PDO::FETCH_ASSOC);

    foreach ($attrs as $attr) {

      $infos = array();
      $infos['nome'] = $attr['nome'];
      $infos['valore'] = $attr['valore_attributo'];
      $infos['descrizione'] = $attr['descrizione'];
      array_push($attributi, $infos);

    }

    return $attributi;

  }

  function getAllUsers(){
      global $objPDO, $usr, $psw;

      if(hasPermissions(1)){
          $userQry = $objPDO->prepare("SELECT u.id, u.nome, u.cognome, u.email, u.codice_fiscale, u.ultimo_accesso, u.password, u.fk_livelli as livello FROM Utenti u WHERE fk_livelli > 1 AND u.stato = 1 AND u.fk_istituto = (SELECT fk_istituto FROM Utenti WHERE email = '$usr' AND password = '$psw') ");
          $userQry->execute();
          $users = $userQry->fetchAll(PDO::FETCH_ASSOC);

          echo json_encode($users);
      }else{
          echo "false";
      }
  }

  function getCategorie(){
      global $objPDO;
      $catQry = $objPDO->prepare("SELECT id, nome_categoria, src_icona FROM Categorie_Elemento");
      $catQry->execute();
      $categorie = $catQry->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode($categorie);
  }


  function getClassiElemento(){

    global $objPDO;

    try{
        $res = $objPDO->prepare("SELECT * FROM Classi_Elemento ORDER BY fk_categoria");
        $res->execute();

        /*il while scorre tutte le righe della query e mette ogni risultato
        nel vettore output che verrà reso in formato json*/
        $output = array();

        //ogni ciclo porta avanti automaticamente il contatore(dalla funzione -->  mysqli_fetch_row)
        $rows = $res->fetchAll(PDO::FETCH_ASSOC);

        //print_r($output);
        echo json_encode($rows);

    }catch(PDOException $e){
        echo "$e";
    }catch(Exception $e){
        echo "$e";
    }
/*
    echo json_encode($output);
		# print_r($output);
*/
  }

?>
