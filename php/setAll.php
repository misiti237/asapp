<?php

  include("./dbconnection.php");
  include("./common.php");

  $req = $_POST["set"];  //il tipo di elemento richiesto (utenti, items, enti, ecc...)
  $usr = $_POST["user"];
  $psw = $_POST["psw"];
  $data = $_POST["data"]; //dati da inserire

  $ret_diz = null;

  switch ($req) {

    case 'updateItem':
        if (hasPermissions(2)) {
			$ret_diz = insertItem();
		}
        break;

    case 'insertPlitem':
        if (hasPermissions(2)) {
			$ret_diz = insertNewPlitem();
		}
        break;

    case 'verifica':
        verificaItem();
        break;

    case 'campo_note':
        salvaCampoNote();
        break;

    case 'eliminaPlitems':
        if (hasPermissions(2)) eliminaPlitems();
        break;

    case 'aggiungiPiano':
        if (hasPermissions(2)) aggiungiPiano();
        break;

    case 'rinominaPiano':
        if (hasPermissions(2)) rinominaPiano();
        break;

    case 'eliminaPiano':
        if (hasPermissions(2)) eliminaPiano();
        break;

    case 'nuovaVerifica':
        azzeraStato();
        break;

    case 'aggiungiFilePlanimetria':
        if (hasPermissions(2)) uploadImagePlanimetria();
        break;

    case 'addEdificio':
        addEdificio();
        break;

	case 'updateEdificio':
		updateEdificio();
		break;

    case 'deleteEdificio':
        if (hasPermissions(2)) delEdificio();
        break;

    case 'azioniUtente':
        if (hasPermissions(1)) actionUser();
        break;

    case 'eliminaUtente':
        if (hasPermissions(1)) delUser();
        break;


    default:

        # code...
        break;

	if ($ret_diz != null) {
		echo json_encode($ret_diz);
	}
  }



function insertNewPlitem(){
	/*
   *   La procedura prende il vecchio plitem, lo disattiva e ne crea uno nuovo
   *   L'id del nuovo plitem verrà usato per inserire i vari elementi
   *   Viene passato array con
   *       arr["actual_id"] = id_vecchio_plitem
   *       arr["zoom_perc"] = nuovo_zoom
   *       arr["items"] = elementi della planimetria
   */

	$ret_val = true;
	$msg = '';

	global $data, $objPDO;

	try {
		$arr = json_decode($data, true);

		$oldPlitemId = $arr["actual_id"];

		//prima prendo src planimetria e fk_piani di plitem attuale
		$res = $objPDO->prepare("SELECT pl.img_planimetria, pl.fk_piani FROM Plitems pl WHERE id = '$oldPlitemId'");
		$res->execute();
		$result = $res->fetch(PDO::FETCH_ASSOC);
		$piano = $result['fk_piani'];
		$img = $result['img_planimetria'];

		//poi disattivo vecchio plitem --> metto stato a 0
		$update = $objPDO->prepare("UPDATE Plitems SET stato = 0 WHERE id = :value");
		$update->bindParam(':value', $oldPlitemId);
		$update->execute();

		//e metto items di plitems vecchio a stato = 0
		$update = $objPDO->prepare("UPDATE Items SET stato = 0 WHERE fk_plitems = :value");
		$update->bindParam(':value', $oldPlitemId);
		$update->execute();

		//poi inserisco nuovo plitem
		$insert = $objPDO->prepare("INSERT INTO Plitems(img_planimetria, fk_piani, zoom_perc) VALUES (:img, :piano, :zoom)");
		$insert->bindParam(':img', $img);
		$insert->bindParam(':piano', $piano);
		$insert->bindParam(':zoom', $arr["zoom_perc"]);
		$insert->execute();

		$newId = $objPDO->lastInsertId();


		foreach($arr["items"] as $element){
			insertItem($element, $newId);
		}

		$ret_val = true;
	} catch(Exception $e) {
		$msg = $e->getMessage();
	}

	return array('ret'=>$ret_val, 'msg'=>$msg);
}



  function insertItem($newItem, $newId){
	  	/*
		*   La procedura prende l'elemento e l'id del plitem in cui deve essere inserito
		*   Distingue due casi:
		*       1) elemento non esistente (nuovo)
		*       2) elemento già esistente
		*/

	  	$ret_val = true;
		$msg = '';

    	global $data,$objPDO,$usr,$psw;

	    //in $data c'è il vettore dell'item da salvare

		try {

			//in $data c'è il vettore dell'item da salvare
    		//$newItem = json_decode($data,true);

		    //converto in decimale le coordinate dell'elemento
		    $posx = number_format($newItem["x"], 2, ".", "");
		    $posy = number_format($newItem["y"], 2, ".", "");

		    //in $newItem["elemento"] guardo se elemento esiste o no
		    if($newItem["elemento"] == null){

		      //inserimento nuovo item
		      $classe_elemento = $newItem["categoriaElemento"];
		      $codice_elemento = $newItem["cod"];

		      /*
		      *     prima inserisco Elemento
		      *     Logica del codice elemento non ancora implementata,
		      *         --> se esiste prende quello vecchio altrimenti mette 000 (lato client)
		      */
		      $inserte = $objPDO->prepare("INSERT INTO Elementi(codice, fk_classe_elemento) VALUES (:cod, :fk)");
		      $inserte->bindValue(':cod', $codice_elemento);
		      $inserte->bindParam(':fk', $classe_elemento);
		      $inserte->execute();

		      $idNewElement = $objPDO->lastInsertId();

		      //poi inserisco item
		      // try{
		        $objPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		        $insert = $objPDO->prepare("INSERT INTO Items(posX, posY, stato, fk_plitems, fk_elemento) VALUES (:posx, :posy, :stato, :pl, :el)");
		        $insert->bindParam(':posx', $posx);
		        $insert->bindParam(':posy', $posy);
		        $insert->bindValue(':stato', 1, PDO::PARAM_INT);
		        $insert->bindValue(':pl', $newId, PDO::PARAM_INT);
		        $insert->bindValue(':el', $idNewElement, PDO::PARAM_INT);
		        $insert->execute();
		      // }catch(PDOException $e){
		      //   echo 'Errors: ' . $e->getMessage();
		      // }

		      //poi inserisco attributi di elemento
		      $attributi = $newItem["attributi"];
		      foreach($attributi as $attr){
		        //prima trovo id attributo
		        $nomeAttr = $attr["nome"];
		        $valAttr = $attr["valore"];
		        $res = $objPDO->prepare("SELECT id FROM Attributi WHERE nome = '$nomeAttr'");
		        $res->execute();
		        $idA = $res->fetchColumn();

		        // try{
				$inserta = $objPDO->prepare("INSERT INTO Attributi_Elementi(valore_attributo, fk_attributo, fk_elemento, stato) VALUES (:valore, :fk_a, :fk_el, :stato)");
		        //la periodicità di verifica se non inserita ha valore di default
		        if($idA == 10){
					if($valAttr == null || $valAttr == ''){
						$valAttr = 3;	//3 mesi
					}
				}
				$inserta->bindParam(':valore', $valAttr);
				$inserta->bindValue(':fk_a', $idA, PDO::PARAM_INT);
				$inserta->bindValue(':stato', 1, PDO::PARAM_INT);
				$inserta->bindValue(':fk_el', $idNewElement, PDO::PARAM_INT);
		      	$inserta->execute();
		        // }catch(PDOException $e){
		        //   echo 'Errors: ' . $e->getMessage();
		        // }

		      }

		    }else{

		      // insert da elemento esistente
		      $insert = $objPDO->prepare("INSERT INTO Items(posX, posY, stato, fk_plitems, fk_elemento) VALUES (:posx, :posy, :stato, :pl, :el)");
		      $insert->bindParam(':posx', $posx);
		      $insert->bindParam(':posy', $posy);
		      $insert->bindValue(':stato', 1, PDO::PARAM_INT);
		      $insert->bindValue(':pl', $newId, PDO::PARAM_INT);
		      $insert->bindValue(':el', $newItem["elemento"], PDO::PARAM_INT);
		      $insert->execute();

		      //id del nuovo item che verrà inserito, serve per la migrazione delle checklist
		      $idItem = $objPDO->lastInsertId();

		      //insert attributi
		      $attributi = $newItem["attributi"];
		      $idE = $newItem["elemento"];   //id elemento
		      $update = $objPDO->prepare("UPDATE Attributi_Elementi ae JOIN Attributi a ON ae.fk_attributo = a.id SET stato = 0 WHERE ae.fk_elemento = :id");
		      $update->bindParam(':id', $idE);
		      $update->execute();

		      foreach($attributi as $attr){
				$nomeAttr = $attr["nome"];
		        $res = $objPDO->prepare("SELECT id FROM Attributi WHERE nome = '$nomeAttr'");
		        $res->execute();
		        $idA = $res->fetchColumn();

				//$insert = $objPDO->prepare("UPDATE Attributi_Elementi ae JOIN Attributi a ON ae.fk_attributo = a.id SET ae.valore_attributo = :valore WHERE a.nome = :nome AND ae.fk_elemento = :id");
		        $insert = $objPDO->prepare("INSERT INTO Attributi_Elementi (valore_attributo, stato, fk_attributo, fk_elemento) VALUES(:valore, :stato, :fk_attributo, :fk_elemento)");
		        // la periodicità di verifica se non inserita ha valore di default
		        if($idA == 10){
					if($attr["valore"] == null || $attr["valore"] == ''){
						$attr["valore"] = 3;	//3 mesi
					}
				}
				$insert->bindParam(':valore', $attr["valore"]);
				$insert->bindValue(':stato', 1, PDO::PARAM_INT);
				$insert->bindParam(':fk_attributo', $idA);
				$insert->bindParam(':fk_elemento', $idE);
				$insert->execute();
		      }

		      //update codice elemento
		      $codice_elemento = $newItem["cod"];
		      $update = $objPDO->prepare("UPDATE Elementi SET codice = :codice WHERE id = :id");
		      $update->bindParam(':codice', $codice_elemento);
		      $update->bindParam(':id', $idE);
		      $update->execute();

		      //migrazione checklist (inserisco in nuovo elemento e metto vecchie con stato a 0)
		      $checklist = $newItem["checklist"];
		      $oldItemId = $newItem["id"];
		      foreach($checklist as $check){
		        $update = $objPDO->prepare("UPDATE Verifiche SET stato = '0' WHERE fk_items = :it AND fk_check = :chk");
		        $update->bindParam(':it', $oldItemId);
		        $update->bindParam(':chk', $check["id"]);
		        $update->execute();
		      };

		      //prendo id utente per aggiornamento verifiche
		      $res = $objPDO->prepare("SELECT id FROM Utenti WHERE email = '$usr' AND password = '$psw'");
		      $res->execute();
		      $usr_id = $res->fetchColumn();

		      foreach($checklist as $check){
		        if($check["valore"] != ""){
		          $insert = $objPDO->prepare("INSERT INTO Verifiche(valore_check, stato, fk_check, fk_items, fk_utenti) VALUES (:val, '1', :chk, :it, :usr)");
		            $insert->bindParam(':val', $check["valore"]);
		            $insert->bindParam(':chk', $check["id"]);
		            $insert->bindParam(':it', $idItem);
		            $insert->bindParam(':usr', $usr_id);
		          $insert->execute();
		        }
		      }

		    }
		  $ret_val = true;
	  } catch(Exception $e) {
		  $msg = $e->getMessage();
	  }

	  return array('ret'=>$ret_val, 'msg'=>$msg);
  }

   //prima metto verifiche vecchie a 0
    // $update = $objPDO->prepare("UPDATE Verifiche SET stato = '0' WHERE fk_items = :value");
    //   $update->bindParam(':value', $single_check[2]);
    //   $update->execute();

  function azzeraStato(){
    global $objPDO, $data;

    $itemsId = $data;

    $update = $objPDO->prepare("UPDATE Verifiche SET stato = '0' WHERE fk_items = :value");
      $update->bindParam(':value', $itemsId);
      $update->execute();
  }

  function verificaItem(){

    global $data,$objPDO,$usr,$psw;

    $single_check = json_decode($data, true);

    if($single_check[0] != "" && $single_check[0] != null){
      $res = $objPDO->prepare("SELECT id FROM Utenti WHERE email = '$usr' AND password = '$psw'");
      $res->execute();
      $usr_id = $res->fetchColumn();

      //inserisco nuove verifiche

      $insert = $objPDO->prepare("INSERT INTO Verifiche(valore_check, stato, fk_check, fk_items, fk_utenti) VALUES (:val, '1', :chk, :it, :usr)");
        $insert->bindParam(':val', $single_check[0]);
        $insert->bindParam(':chk', $single_check[1]);
        $insert->bindParam(':it', $single_check[2]);
        $insert->bindParam(':usr', $usr_id);
      $insert->execute();

    }

  }


  function eliminaPlitems(){
  	global $data,$objPDO;

  	$update = $objPDO->prepare("UPDATE Plitems SET stato = 0 WHERE id = :value");
      $update->bindParam(':value', $data);
      $update->execute();

  	$update = $objPDO->prepare("UPDATE Items SET stato = 0 WHERE fk_plitems = :value");
      $update->bindParam(':value', $data);
      $update->execute();
  }

  function rinominaPiano(){
    global $data,$objPDO;

	//$values[0] = idpiano
	//$values[1] = nome piano
	$values = json_decode($data, true);

	$update = $objPDO->prepare("UPDATE Piani SET nome = :name WHERE id = :value");
    $update->bindParam(':value', $values[0]);
    $update->bindParam(':name', $values[1]);
    $update->execute();

  }

  function eliminaPiano(){
  	global $data,$objPDO;

  	//$values[0] = idpiano
  	//$values[1] = idplitems
  	$values = json_decode($data, true);

  	$update = $objPDO->prepare("UPDATE Piani SET stato = 0 WHERE id = :value");
      $update->bindParam(':value', $values[0]);
      $update->execute();

  	$update = $objPDO->prepare("UPDATE Plitems SET stato = 0 WHERE id = :value");
      $update->bindParam(':value', $values[1]);
      $update->execute();

  	$update = $objPDO->prepare("UPDATE Items SET stato = 0 WHERE fk_plitems = :value");
      $update->bindParam(':value', $values[1]);
      $update->execute();
  }

  function aggiungiPiano(){
    	global $data,$objPDO;

    	//$values[0] = nomePiano
    	//$values[1] = fk_edificio
    	//$values[2] = srcPlanimetria
    	$values = json_decode($data, true);

      $insert = $objPDO->prepare("INSERT INTO Piani(nome, fk_edifici, stato) VALUES (:nome, :edificio, 1)");
        $insert->bindParam(':nome', $values[0]);
        $insert->bindParam(':edificio', $values[1]);
	  if($insert->execute()){
		  $idPiano = $objPDO->lastInsertId();
		  $insert = $objPDO->prepare("INSERT INTO Plitems(stato, fk_piani, img_planimetria) VALUES (1, :idPiano, :img)");
			$insert->bindParam(':idPiano', $idPiano);
			$insert->bindParam(':img', $values[2]);
		  if($insert->execute()){
			  echo "true";
			  return;
		  }else{
			  echo "false";
			  return;
		  }
	  }else{
		  echo "false";
		  return;
	  }


  }


  function addEdificio(){
      global $objPDO, $data, $usr, $psw;

      $values = json_decode($data, true);

      //prima prendo id istituto
      $res = $objPDO->prepare("SELECT fk_istituto FROM Utenti WHERE email = '$usr' AND password = '$psw'");
      $res->execute();
      $idIsti = $res->fetchColumn();
      $insert = $objPDO->prepare("INSERT INTO Edifici(nome, fk_istituto, stato, codice_miur, indirizzo, civico, cap) VALUES (:nome, :fk, 1, :miur, :indirizzo, :civico, :cap)");
        $insert->bindParam(':nome', $values[0]);
        $insert->bindParam(':miur', $values[1]);
        $insert->bindParam(':indirizzo', $values[2]);
        $insert->bindParam(':civico', $values[3]);
        $insert->bindParam(':cap', $values[4]);
        $insert->bindParam(':fk', $idIsti);
        if($insert->execute()){
            echo "true";
        }else{
            echo "false";
        }
  }


  function updateEdificio(){
      global $objPDO, $data, $usr, $psw;

      $values = json_decode($data, true);



      $insert = $objPDO->prepare("UPDATE Edifici SET nome = :nome, codice_miur = :miur, indirizzo = :indirizzo, civico = :civico, cap = :cap WHERE id = :id_edificio");
        $insert->bindParam(':nome', $values[0]);
        $insert->bindParam(':miur', $values[1]);
        $insert->bindParam(':indirizzo', $values[2]);
        $insert->bindParam(':civico', $values[3]);
        $insert->bindParam(':cap', $values[4]);
        $insert->bindParam(':id_edificio', $values[5]);


		if($insert->execute()){
			echo "true";
		}else{
			echo "false";
		}

  }

  function delEdificio(){
      global $objPDO, $data, $usr, $psw;
      if(hasPermissions(2)){
          //elimino edificio
          $delete = $objPDO->prepare("UPDATE Edifici SET stato = 0 WHERE id = :id");
          $delete->bindParam(':id', $data);
          if($delete->execute()){
              //prendo i piani
              $res = $objPDO->prepare("SELECT id FROM Piani WHERE fk_edifici = :id AND stato = 1");
              $res->bindParam(':id', $data);
              $res->execute();
              $piani = $res->fetchAll(PDO::FETCH_ASSOC);

              foreach ($piani as $piano) {
                  //prendo i plitems
                  $res = $objPDO->prepare("SELECT id FROM Plitems WHERE fk_piani = :id AND stato = 1");
                  $res->bindParam(':id', $piano["id"]);
                  $res->execute();
                  $plitems = $res->fetchAll(PDO::FETCH_ASSOC);

                  foreach ($plitems as $plit) {
                      //elimino items
                      $delete = $objPDO->prepare("UPDATE Items SET stato = 0 WHERE fk_plitems = :id");
                      $delete->bindParam(':id', $plit["id"]);
                      $delete->execute();
                  }

                  //elimino plitems
                  $delete = $objPDO->prepare("UPDATE Plitems SET stato = 0 WHERE fk_piani = :id AND stato = 1");
                  $delete->bindParam(':id', $piano["id"]);
                  $delete->execute();
                  echo $piano["id"];

              }
              //elimino piano
              $delete = $objPDO->prepare("UPDATE Piani SET stato = 0 WHERE fk_edifici = :id AND stato = 1");
              $delete->bindParam(':id', $data);
              $delete->execute();


          }else{
              echo "false";
          }
      }else{
          echo "false";
      }
      echo "true";

  }

    function delUser(){
        global $objPDO, $data, $usr, $psw;

        $delete = $objPDO->prepare("UPDATE Utenti SET stato= 0 WHERE id = :id");
        $delete->bindParam(':id', $data);
        if($delete->execute()){
            echo "true";
        }else{
            echo "false";
        }

    }

    function actionUser(){
        global $objPDO, $data, $usr, $psw;

        // if(hasPermissions(1)){
            /*
            arr[0] = req;
        	arr[1] = nome;
        	arr[2] = cogn;
        	arr[3] = email;
        	arr[4] = psw;
        	arr[5] = cod_fisc;
        	arr[6] = livello;
            */
            $newUser = json_decode($data, true);
            $res = $objPDO->prepare("SELECT fk_istituto FROM Utenti WHERE email = '$usr' AND password = '$psw'");
            $res->execute();
            $idIsti = $res->fetchColumn();

            $res = $objPDO->prepare("SELECT id FROM Livelli WHERE descrizione = :livello");
            $res->bindParam(':livello', $newUser[6]);
            $res->execute();
            $livello = $res->fetchColumn();

            if($newUser[0]=='add'){
                //controllo se email esiste gia o no
                // $res = $objPDO->prepare("SELECT COUNT(*) FROM Utenti WHERE email = :mail");
                // $res->bindParam(":mail", $newUser[3]);
                // $res->execute();
                // $email = $res->fetchColumn();
                // if($email == 0){
                    $insert = $objPDO->prepare("INSERT INTO Utenti(nome, cognome, email, password, codice_fiscale, fk_livelli, fk_istituto) VALUES (:nome, :cogn, :email, :psw, :cod_fisc, :fk_lvl, :fk_isti)");
                        $insert->bindParam(':nome', $newUser[1]);
                        $insert->bindParam(':cogn', $newUser[2]);
                        $insert->bindParam(':email', $newUser[3]);
                        $insert->bindParam(':psw', $newUser[4]);
                        $insert->bindParam(':cod_fisc', $newUser[5]);
                        $insert->bindParam(':fk_lvl', $livello);
                        $insert->bindParam(':fk_isti', $idIsti);
                    if($insert->execute()){
                        echo "true";
                    }else{
                        echo "false";
                    }
                // }else{
                //     echo "false";
                // }
            }else if($newUser[0]=='mod'){
                $update = $objPDO->prepare("UPDATE Utenti SET nome = :nome, cognome = :cogn, password = :psw, codice_fiscale = :cod_fisc, fk_livelli = :fk_lvl WHERE email = :email");
                    $update->bindParam(':nome', $newUser[1]);
                    $update->bindParam(':cogn', $newUser[2]);
                    $update->bindParam(':email', $newUser[3]);
                    $update->bindParam(':psw', $newUser[4]);
                    $update->bindParam(':cod_fisc', $newUser[5]);
                    $update->bindParam(':fk_lvl', $livello);
                if($update->execute()){
                    echo "true";
                }else{
                    echo "false";
                }
            }else{
                echo "false";
            }
        // }else{
        //     echo "false";
        // }
    }


    function salvaCampoNote(){
        global $objPDO, $data, $usr, $psw;

        $arr = json_decode($data, true);

        $note = $arr[0];
        $idE = $arr[1];

        $update = $objPDO->prepare("UPDATE Attributi_Elementi SET stato = 0 WHERE fk_attributo = :fk_attributo AND fk_elemento = :fk_elemento");
        $update->bindValue(':fk_attributo', 11, PDO::PARAM_INT);    //attributo campo note
        $update->bindParam(':fk_elemento', $idE);
        if(!$update->execute()){
            echo "false";
            return;
        }

        $insert = $objPDO->prepare("INSERT INTO Attributi_Elementi (valore_attributo, stato, fk_attributo, fk_elemento) VALUES(:valore, :stato, :fk_attributo, :fk_elemento)");
        $insert->bindParam(':valore', $note);
        $insert->bindParam(':fk_elemento', $idE);
        $insert->bindValue(':stato', 1, PDO::PARAM_INT);
        $insert->bindValue(':fk_attributo', 11, PDO::PARAM_INT);    //attributo campo note
        if($insert->execute()){
            echo "true";
        }else{
            echo "false";
        }

    }
/*
	function uploadImagePlanimetria() {
		global $_FILES;
		$target_dir = "../plitems/";
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

		// Check if file already exists
		if (file_exists($target_file)) {
			echo "false";
		}

		// Check file size
		if ($_FILES["file"]["size"] > 800000) {
			echo "false";
		}

		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
			echo "false";
		}


		// if everything is ok, try to upload file
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
			echo "true";
		} else {
			echo "false";
		}
    }
*/


?>
