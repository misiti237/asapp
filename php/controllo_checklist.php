<?php

    /*
        Prendo verifiche con stato = 1 (verifiche già effettuate) e con valore_check!=0 (verfiche gialle/rosse) 
        Poi prendo tutti gli elementi che non hanno una verifica attiva, ovvero quelli non verificati

        Ancora non faccio controlli sulla decorrenza dall'ultima verifica -->(li faccio con il cron job)
    */

    include("./dbconnection.php");

    $usr = $_POST["user"];
    $psw = $_POST["psw"];
    $data = $_POST["data"];


    $res = $objPDO->prepare("SELECT fk_istituto FROM Utenti WHERE email = '$usr' AND password = '$psw'");
    $res->execute();
    $idIsti = $res->fetchColumn();

    $output = array();

    function checkChecklist(){
        global $idIsti, $objPDO, $output;
        //Prendo elementi con verifica non valida (valori = 0 o non compilati)

        //$res = $objPDO->prepare("SELECT v.id, v.fk_items, v.data_creazione, chk.nome_check, ae.valore_attributo, e.nome as edificio_nome, p.nome as piano_nome, el.codice, ce.nome_classe AS tipo_elemento FROM Items it, Verifiche v, Plitems pl, Piani p, Edifici e, Istituti i, Elementi el, Attributi_Elementi ae, Attributi a, Checklist chk, Classi_Elemento ce WHERE i.id = :istit AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND it.fk_plitems = pl.id AND v.fk_items = it.id AND v.stato = 1 AND it.fk_elemento = el.id AND ae.fk_elemento = el.id AND ae.fk_attributo = a.id AND a.nome = 'PerChk-Mesi' AND v.fk_check = chk.id AND el.fk_classe_elemento = ce.id");
        //$res = $objPDO->prepare("SELECT v.fk_items, el.data_creazione, ae.valore_attributo, e.nome as edificio_nome, p.nome as piano_nome, el.codice, ce.nome_classe AS tipo_elemento FROM Items it, Verifiche v, Plitems pl, Piani p, Edifici e, Istituti i, Elementi el, Attributi_Elementi ae, Attributi a, Checklist chk, Classi_Elemento ce WHERE (i.id = :istit AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND it.fk_plitems = pl.id AND v.fk_items = it.id AND v.stato = 1 AND it.fk_elemento = el.id AND ae.fk_elemento = el.id AND ae.fk_attributo = a.id AND a.nome = 'Scadenza' AND v.fk_check = chk.id AND el.fk_classe_elemento = ce.id AND v.valore_check != 0) GROUP BY v.fk_items");
        
        $res = $objPDO->prepare("SELECT v.fk_items, el.data_creazione, e.nome as edificio_nome, p.nome as piano_nome, el.codice, ce.nome_classe AS tipo_elemento FROM Items it, Verifiche v, Plitems pl, Piani p, Edifici e, Istituti i, Elementi el, Checklist chk, Classi_Elemento ce WHERE (i.id = :istit AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND it.fk_plitems = pl.id AND v.fk_items = it.id AND v.stato = 1 AND it.stato = 1 AND it.fk_elemento = el.id AND v.fk_check = chk.id AND el.fk_classe_elemento = ce.id AND v.valore_check != 0) GROUP BY v.fk_items");
        
        $res->bindParam(":istit", $idIsti);
        $res->execute();

        $rows= $res->fetchAll(PDO::FETCH_ASSOC);

        $date_now = date("Y-m-d H:i:s");
        foreach ($rows as $row) {
            //
            // //$scadenza = strtotime(date("Y-m-d", strtotime($row['data_creazione'])) . " +1 month");
            //
            // //valore attributo = gap scadenza in mesi
            // if(!$row['valore_attributo'] || $row['valore_attributo']=="") {
            //     $aggiunta = 3; //valore default 1 mese
            // }else{
            //     $aggiunta = $row['valore_attributo'];
            // }
            //
            // $scadenza = date('Y-m-d', strtotime($row['data_creazione'].' +'.$aggiunta.' months'));
    		// // $scadenza = date('Y-m-d', strtotime($row['data_creazione'].' + 25 days'));
    		// // $scadenza = date('Y-m-d', strtotime("+".$aggiunta." months", $row['data_creazione']));
            //
            // $buffer = array();
            //
            // //questo controllo è per la scadenza dell'elemento
            // if($date_now > $scadenza){
            //     $buffer['elemento_scaduto'] = true;
            // }else{
            //     $buffer['elemento_scaduto'] = false;
            // }

            $buffer['edificio'] = $row['edificio_nome'];
            $buffer['piano'] = $row['piano_nome'];
            $buffer['codice'] = $row['codice'];
            $buffer['data_creazione'] = $row['data_creazione'];
            // $buffer['aggiunta'] = $aggiunta;
            $buffer['tipo'] = $row['tipo_elemento'];

            array_push($output, $buffer);

        }
    }
    // SELECT it.id, el.codice, ce.nome_classe as strumento, e.nome as edificio, p.nome as piano FROM Items it, Plitems pl, Piani p, Edifici e, Istituti i, Elementi el, Classi_Elemento ce WHERE (i.id = 3 AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND it.fk_plitems = pl.id AND it.fk_elemento = el.id AND el.fk_classe_elemento = ce.id AND it.stato = 1 AND it.id NOT IN (SELECT fk_items FROM Verifiche WHERE stato = 1)) GROUP BY it.id
    //
    function elementiNonVerificati(){
        global $idIsti, $objPDO, $output;

		//prendo item che hanno lo stato della verifica = 0
        $res = $objPDO->prepare("SELECT it.id, el.codice, el.data_creazione, ce.nome_classe as tipo_elemento, e.nome as edificio_nome, p.nome as piano_nome FROM Items it, Plitems pl, Piani p, Edifici e, Istituti i, Elementi el, Classi_Elemento ce WHERE (i.id = :istit AND e.fk_istituto = i.id AND p.fk_edifici = e.id AND pl.fk_piani = p.id AND it.fk_plitems = pl.id AND it.fk_elemento = el.id AND el.fk_classe_elemento = ce.id AND it.stato = 1 AND it.id NOT IN (SELECT fk_items FROM Verifiche WHERE stato = 1)) GROUP BY it.id");
        $res->bindParam(":istit", $idIsti);
        $res->execute();

        $rows= $res->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $row) {
            $buffer = array();

            $buffer['edificio'] = $row['edificio_nome'];
            $buffer['piano'] = $row['piano_nome'];
            $buffer['codice'] = $row['codice'];
            $buffer['data_creazione'] = $row['data_creazione'];
            $buffer['tipo'] = $row['tipo_elemento'];
            array_push($output, $buffer);

        }
    }

    checkChecklist();
    elementiNonVerificati();


    echo json_encode($output);

?>
