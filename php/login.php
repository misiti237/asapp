<?php
    include("./dbconnection.php");

    $usr = $_POST["username"];
    $psw = $_POST["password"];
    header("Access-Control-Allow-Origin: *");

    $sql = "SELECT password FROM Utenti WHERE email = '$usr'";
    $res = $objPDO->prepare($sql);
    $res->execute();
    //in check finirà il numero di colonne della query
    $check = $res->rowCount();
    if($check == 0){
        //nessun utente trovato, il conto delle righe della query ha restituito 0
        echo 404;
    }else if($check == 1){
        //la query ha restituito una sola riga, esiste una sola password per quell'utente
        if($psw == $res->fetchColumn()){
            //utente trovato e password corrispondente
            $ultimo_accesso = date("Y-m-d H:i:s");
            $update = $objPDO->prepare("UPDATE Utenti SET ultimo_accesso = :now WHERE email = :usr AND password = :psw");
                $update->bindParam(':now', $ultimo_accesso);
                $update->bindParam(':usr', $usr);
                $update->bindParam(':psw', $psw);
            if($update->execute()) echo 200;
        }else {
            //password sbagliata
            echo 403;
        }
    }

?>
