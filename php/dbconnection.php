<?php
    include("./config.php");
    //dbconn
    $utente = $MYSQL_USER;
    $pass = $MYSQL_PSSW;
    $host = $MYSQL_HOST;
    $db = $MYSQL_DBAS;
    //DSN fisso per il pdo
    $dsn = "mysql:host=$host;dbname=$db;charset=UTF8;";
    //Creo pdo
    header("Access-Control-Allow-Origin: *");
    try{
        $objPDO = new PDO($dsn,$utente,$pass);
        //echo "connessione avvenuta con successo";
    }catch(PDOException $e){
        echo "errore di connessione: ".$e->getMessage();
        exit();
    }
    //disconnessione
    //$objPDO = null;
?>
